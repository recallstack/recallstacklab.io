---
title: Portafolio
draft: false
nocomments: true
date: Th/05/yyyyT15:22
---
Aquí tienes algunos de los proyectos con los que más he colaborado.

## Odoo

Software de código abierto de gestión empresarial integral.

* [Mis contribuciones](https://github.com/odoo/odoo/commits?author=Yajo)
* [Formé parte del equipo que ganó el premio de mejores contribuidores 2017](https://twitter.com/Odoo/status/915979619697819648).
* [Nominado como mejor contribuidor en 2018](https://www.odoo.com/es_ES/blog/nuestro-blog-5/post/presentando-a-los-nominados-de-este-ano-para-los-premios-de-odoo-experience-2018-505).

## Doodba

Una familia de proyectos destinada a hacer que desplegar [Odoo](https://www.odoo.com/) sea coser y cantar. Uno de los proyectos pioneros en el uso de contenedores para eso. He sido el principal diseñador y mantenedor de este proyecto.

* https://github.com/Tecnativa/doodba
* https://github.com/Tecnativa/doodba-copier-template
* https://github.com/Tecnativa/doodba-qa

## Copier

Generador de plantillas avanzado con buen soporte para actualizaciones y múltiples plantillas. Soy comantenedor. \[Lee por qué]({{< relref "/post/copier-v3-introduccion.md" >}}).

* https://github.com/copier-org/copier
* https://github.com/Tecnativa/doodba-copier-template
* https://github.com/Tecnativa/image-template
* https://gitlab.com/moduon/odoo-module-template

## Imagenes Docker

* https://github.com/Tecnativa/docker-socket-proxy - Desata el poder de exponer tu socket de Docker sin desatar la inseguridad.
* https://github.com/Tecnativa/docker‐whitelist - Un servicio netcat para permitir solo ciertas conexiones de red al exterior

## Flathub

Soy mantenedor en algunos proyectos:

* [KmCaster](https://github.com/flathub/com.whitemagicsoftware.kmcaster)
* [Not Tetris 2](https://github.com/flathub/net.stabyourself.nottetris2)

## Este blog

El blog que estás leyendo. Puedes mandarme parches si lo ves oportuno.

* https://gitlab.com/recallstack/recallstack.gitlab.io

## Logros diversos

Aquí incluyo algunos logros que he ido obteniendo. Me parece interesante compartirlos, a modo de currículo.

* Diciembre 2020. [Me entrevistan en Python Podcast para halblar sobre Copier](https://www.pythonpodcast.com/copier-project-scaffolding-episode-297/).
* Febrero 2020. [A la comunidad le gusta mi proyecto Doodba](https://twitter.com/PCatinean/status/1225795167103455234).
* Enero 2020. [Voluntariamente me nombran comantenedor de Copier](https://github.com/copier-org/copier/pull/109#issuecomment-576425526).
* Junio 2019. [Programo un complejo convertidor de Bootstrap v3 a v4 para las migraciones de Odoo de v11 a v12](https://github.com/OCA/openupgradelib/pull/148).
* Enero 2019. [Mejora de rendimiento del 80% en el módulo `contract` de Odoo](https://github.com/OCA/contract/pull/266).
* Octubre 2019. [Programo una forma de actualizar Odoo sin downtime](https://github.com/acsone/click-odoo-contrib/pull/38).
* Octubre 2018. [Migración con refactorización épica del módulo `web_responsive`](https://github.com/OCA/web/pull/1066).
* Agosto 2018. [Nominado como mejor contribuidor en 2018](https://www.odoo.com/es_ES/blog/nuestro-blog-5/post/presentando-a-los-nominados-de-este-ano-para-los-premios-de-odoo-experience-2018-505) (repetido arriba, ya lo sé).
* Mayo 2018. [Ayudo a diagnosticar un grave error de 3 años de antigüedad](https://github.com/wkhtmltopdf/wkhtmltopdf/issues/2711#issuecomment-389155264).
* Diciembre 2017. [Desarrollo el módulo `website_form_builder`](https://github.com/OCA/website/pull/402).
* Octubre 2017. [Formé parte del equipo que ganó el premio de mejores contribuidores 2017](https://twitter.com/Odoo/status/915979619697819648) (repetido también).
* Junio 2017. [Ponencia sobre cómo usar Doodba para lanzar un proyecto personalizado de Odoo en 10 minutos](https://www.youtube.com/watch?v=d8wIygoGuuM).
* Agosto 2016. [Demuestro y arreglo un grave error en el ORM de Odoo](https://github.com/odoo/odoo/pull/13082).
* Diciembre 2011. [Mi breve colaboración con el blog "Usemos Linux", explicando cómo hacer un paquete RPM](http://usemoslinux.blogspot.com/2011/12/empaquetado-rpm-parte-4-empaquetando.html).

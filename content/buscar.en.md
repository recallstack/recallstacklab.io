---
title: "Search"
slug: search
nocomments: true
date: 2018-12-21T21:59:25+01:00
---

Search in RecallStack using [DuckDuckGo](https://duckduckgo.com/):

<iframe
  src="https://duckduckgo.com/search.html?site=recallstack.icu&prefill=Search in RecallStack&focus=yes"
  style="overflow:hidden;margin:0;padding:0;width:100%;height:40px;"
  frameborder="0"></iframe>

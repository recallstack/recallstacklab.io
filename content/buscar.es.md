---
title: "Buscar"
nocomments: true
date: 2018-12-21T21:59:25+01:00
---

Busca en RecallStack usando [DuckDuckGo](https://duckduckgo.com/):

<iframe
  src="https://duckduckgo.com/search.html?site=recallstack.icu&prefill=Buscar en RecallStack&focus=yes"
  style="overflow:hidden;margin:0;padding:0;width:100%;height:40px;"
  frameborder="0"></iframe>

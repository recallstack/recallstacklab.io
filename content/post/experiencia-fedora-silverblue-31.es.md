---
title: "Experiencia Fedora Silverblue 31"
date: 2020-02-09T11:55:35Z
draft: true
tags:
  - fedora
  - silverblue
  - linux
---

Recientemente he estado probando Fedora 31 Silverblue. ¿Qué tiene de especial? ¿Qué tal
va? Lee a continuación...

<!-- more -->

Una distribución Linux tradicional consta de:

1. Un gestor de paquetes (rpm, apt...).
1. Un repositorio central (o varios) donde se colocan todos los paquetes que conforman
   el sistema operativo.
1. Una colección seleccionada de paquetes (por ejemplo: kernel, GNOME y sus componentes
   base...) que conforman el _sistema base_.
1. Actualizaciones progresivas, paquete a paquete.

El problema con este sistema es que no es determinístico. No puedes saber qué paquetes
habrá descargado cada persona en su ordenador, ni si todos los que hay son compatibles
al 100% entre sí. Existen sistemas que intentan hacer que esto sea lo más fiable
posible, pero es complicado que no haya un posible error.

Otro problema es que limita las actualizaciones. Si tu aplicación depende de la librería
`libalgo-2.x` y tu sistema operativo proporciona la `libalgo-1.x`, no puedes
proporcionar tu aplicación para ese sistema salvo que hagas un montón de workarounds.

Para solucionar este último problema existe [Flatpak](https://www.flatpak.org/), que
viene siendo "Docker para aplicaciones de escritorio", para entendernos.

**Flatpak, hoy en día, funciona superbien**. Hasta existe
[FlatHub](https://flathub.org/), donde tienes un repositorio central de aplicaciones
_instalables en cualquier distro Linux_.

Claro, si ahora las aplicaciones de escritorio se pueden empaquetar como un contenedor
con todas sus dependencias necesarias... ¿No podríamos hacer eso mismo con el sistema
operativo? Pues bien, te presento a...

{{< figure src="https://silverblue.fedoraproject.org/public/silverblue-logo.svg" title="Fedora 31 Silverblue" link="https://silverblue.fedoraproject.org/" class="float-left" >}}

Fedora Silverblue es como se le llama a un nuevo sabor de sistema operativo que se
construye de forma diferente a los tradicionales:

- El sistema operativo es una imagen congelada con una selección a mano de dependencias.
  Incluye el kernel y todo un entorno básico GNOME (por ahora).
- El gestor de paquetes es `rpm-ostree`, que permite...
- ... que si quieres instalar otros paquetes RPM, lo que haces en realidad es
  "rebasarlos" (estilo git) sobre la imagen del SO, construyendo una nueva imagen para
  ti.

La idea es que la mayoría de aplicaciones las instales con Flatpak, pero puedes añadir
RPM si lo necesitas.

Así que con Silverblue, lo que seleccionas en GRUB al arrancar no es el kernel sobre el
que correrá tu SO, sino el kernel + todo el conjunto que conforma un sistema operativo
de escritorio moderno (GNOME y compañía).

Si algo falla, simplemente usa la imagen anterior y sigue trabajando.

Dado que los flatpaks se pueden actualizar en caliente (lo único que necesitas es cerrar
la aplicación y volver a abrirla para tener la versión actualizada) y que la nueva
imagen también se descarga y construye mientras estás usando otra, se acabó eso de
esperar durante los reinicios para actualizar el sistema. La nueva imagen actualizada se
construye paralelamente, y luego simplemente reinicias a esa nueva versión y listo.
Exactamente lo mismo pasa al actualizar a versiones nuevas de Fedora. ¡Genial! 🎉

Parece todo maravilloso, pero tiene sus contras:

1. La curva de aprendizaje, si ya estabas acostumbrado a lo tradicional, incluyendo un
   propio Fedora normal.

1. Si quieres instalar un programa con RPM, para usarlo tienes que reiniciar.

1. Instalar dependencias de desarrollo se complica.

   Para esto han creado
   [`toolbox`](https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/), genial
   también, haciendo que tu labor de desarrollo de software se ejecute dentro de
   contenedores también.

1. Y, el principal contra: **no es muy estable**.

Así que es una tecnología prometedora que se puede usar _hoy_ para todo, pero que no
recomiendo para equipos de producción todavía. Tardarás bastante peleando con él y
todavía no está listo para ponérselo a tu abuela.

Pero, no nos equivoquemos: cuando esto sea estable, **probablemente se convierta en la
forma _por defecto_ de construir distros Linux, y nos preguntaremos por qué no se hacía
así desde el principio**...

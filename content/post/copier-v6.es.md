---
title: Copier 6
date: 2022-05-15T08:17:01+01:00
tags:
  - copier
  - scaffolding
---

¡Por fin ha llegado
[Copier 6](https://github.com/copier-org/copier/releases/tag/v6.0.0)! Te cuento las
novedades.

También puede que estés pensando si ofrecerte como voluntario para mantener algún
proyecto de código abierto. Te cuento cómo ha sido esta experiencia, por si te sirve
para decidir.

<!-- more -->

## Novedades

Refactoricé todo el núcleo del programa. Ahora es mucho más mantenible. Gracias a que
había tantos tests, he podido comprobar que todo sigue funcionando bien.

Ahora se usa la sintaxis estándar de Jinja por defecto. Así las plantillas Copier se
integran mejor con el IDE, y la documentación de Jinja se hace más entendible.

Hay una nueva TUI para rellenar formularios. Permite cosas como resaltar sintaxis, mover
el cursor, preguntas condicionales, placeholders, respuestas de varias líneas...

También quité la opción `--subdirectory` porque casi todo el mundo esperaba que
funcionase como en Cookiecutter. Pero es otra historia diferente. Es mejor que se maneje
únicamente en la plantilla, donde quien la desarrolló sabe lo que hace.

Ha habido muchas actualizaciones en la documentación.

Por último, ahora hay
[un registro de cambios actualizado](https://github.com/copier-org/copier/blob/master/CHANGELOG.md),
así que para saber más novedades ese es el mejor lugar que puedes mirar.

## Cómo ha sido el proceso

Pues... para empezar, bastante largo. Copier 5.1.0 salió en 2020. Ha habido 10 alphas y
1 beta antes de llegar a esto.

¿Por qué tanto tiempo? A partir de esa versión, **todos estos cambios los he ido
implementando en mi tiempo libre**.

Algo que hice para mejorar
[esta falta de financiación](https://opensource.guide/getting-paid/) es
[habilitar mi página de sponsors en Github](https://github.com/sponsors/yajo).
¿Funcionó?

- Por una parte, he ganado en total 0,01 € por cada línea de código añadida en todo este
  tiempo. Obviamente, Copier seguirá siendo un hobby, porque de eso no se vive.
- Por otra parte, me conmueve que algunos usuarios hayan querido expresar su gratitud de
  esta forma, y **lo agradezco inmensamente**.

¿Qué lectura tengo de esta situación?

- Una de las mejores maneras de mostrar tu gratitud por un proyecto de código abierto es
  con donaciones.
- Cuando abras tu próxima incidencia, piensa en esto y sé amable. Quien hizo ese
  programa ha dedicado muchas horas para que tú puedas usarlo. Dedícale tú unos minutos
  a leer la documentación, escribir una buena incidencia o abrir un buen PR.
- Si tienes una empresa en la que se usa software libre, una vez al año párate a pensar:
  - ¿Qué proyectos de software libre son más importantes para mi empresa?
  - ¿Cuántas horas de trabajo ahorramos al usarlos?
  - ¿Cuánto cobraría uno de mis trabajadores por cada una de esas horas?
  - Entonces, multiplica. Sabrás cuánto vale ese software para ti.
  - Decide cuánto quieres donar a quien lo mantiene. ¿Tal vez el 10% de eso? ¿El 1%? Lo
    que sea, ayudará a que ese software siga siendo mantenible y tu empresa siga
    ahorrando dinero. Además, te sentirás mejor por estar mostrando tu gratitud, y el
    mantenedor también.

Así que lo recalco: **¡muchas gracias a mis fabulosos donantes!** 💌

Ahora cambiemos de tema y hablemos de la comunidad. En este periodo ha habido 27
contribuidores de código. Además, abrí
[el foro](https://github.com/copier-org/copier/discussions) y la comunidad ha podido
conversar ahí. He podido ver cómo algunos miembros de la comunidad resolvían las dudas
de otros.

Puedo decir que hasta el momento estoy bastante contento con cómo está funcionando la
comunidad de Copier.

Algunos consejos de lo que he aprendido como mantenedor:

- Cierra las incidencias lo antes posible. El estado natural de una incidencia es estar
  resuelta. Si no lo está, es algo excepcional.
- Si no se puede resolver ahora mismo, ten un proceso claro de triaje. Tal vez ponerles
  etiquetas o hitos. Tener las incidencias bien ordenadas es clave para aprovechar el
  poco tiempo que le puedo dedicar al proyecto.
- Agradece siempre las contribuciones. Si la gente se siente a gusto, seguirá
  contribuyendo.
- Guía a los contribuidores. Una pregunta clara o un comentario guiador breve acompañado
  de un enlace acertado suele ser más eficiente que entrar en un largo debate sobre si
  han leído o no toda la documentación.
- Aprovecha las ayudas al contribuidor. Cosas como plantillas de incidencias o de PR
  ayudan mucho.

¡Que disfrutes de Copier 6!

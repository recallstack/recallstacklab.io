---
title: Now also in English!
date: 2020-03-01T11:18:29.000Z
slug: now-also-english
nocomments: false
---
From this moment, this blog is multilang, and new posts will be written in both English
and Spanish simultaneously... if I want. 😛
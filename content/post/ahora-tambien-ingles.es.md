---
title: ¡Ahora También en Inglés!
draft: false
nocomments: false
date: 2020-03-01T11:18:29.000Z
---
Desde este momento, este blog es multiidioma, y las nuevas entradas se escribirán en
inglés y en español simultáneamente... si me apetece. 😛
---
title: "Cómo leer un fichero en una variable en Fish"
date: 2019-01-11T20:15:28Z
categories:
  - Programación
tags:
  - bash
  - Fish
  - scripts
  - Shell
---

Si usas [Fish](http://fishshell.com/) como tu shell, [cosa que
recomiendo]({{< ref "fish-shell" >}}), tal vez te hayas topado con el problema de que no
puedes hacer una cosa tan sencilla en Bash como:

```bash
una_variable="$(cat un_archivo_con_varias_lineas.txt)"
echo "$una_variable"
```

<!--more-->

El comportamiento de nuestras shells a la hora de leer y escribir espacios en blanco
podría sorprendernos, es por eso que he decidido grabar este ejemplo para que veas cómo
lo hace Bash en este caso:

<script id="asciicast-220911" src="https://asciinema.org/a/220911.js" async></script>

En fish, estos comandos no servirían exactamente, ya que la sintaxis `$()` no está
soportada en Fish, y esto es así por diseño. En su lugar se usan paréntesis sin el dólar
`()`, pero tiene la dificultad de que no puede ir directamente dentro de una cadena de
texto `"entre comillas dobles"`, ya que se interpreta como un paréntesis normal y
corriente, sin significado especial.

Pues bien, la solución es bien sencilla:

```fish
cat un_archivo_con_varias_lineas.txt | read -z una_variable
echo "$una_variable"
```

Puedes verlo en ~~directo~~ diferido:

<script id="asciicast-HSH9sTatinkktpd7QHwXzNY4W" src="https://asciinema.org/a/HSH9sTatinkktpd7QHwXzNY4W.js" async></script>

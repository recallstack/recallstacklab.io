---
title: Copier 6
date: 2022-05-15T08:17:01+01:00
tags:
  - copier
  - scaffolding
---

[Copier 6](https://github.com/copier-org/copier/releases/tag/v6.0.0) has finally
arrived! I'll tell you what's new.

You may also be thinking about volunteering to maintain an open source project. I'll
tell you what this experience has been like, in case it helps you to decide.

<!-- more -->

## What's new

I refactored the whole core of the program. Now it is much more maintainable. Thanks to
the fact that there were so many tests, I could check that everything still works fine.

Now the standard Jinja syntax is used by default. So the Copier templates are better
integrated with your IDE, and the Jinja documentation is more understandable.

There is a new TUI for form filling. It allows things like syntax highlighting, cursor
moving, conditional questions, placeholders, multi-line answers...

I also removed the `--subdirectory` option because most people expected it to work like
in Cookiecutter. But it is a different story. It is better to be handled only in the
template, where whoever developed it knows what they are doing.

There have been many updates to the documentation.

Finally, there is now
[an updated changelog](https://github.com/copier-org/copier/blob/master/CHANGELOG.md),
so for more news that's the best place to look.

## How has the process been

Well... to start with, quite a long one. Copier 5.1.0 came out in 2020. There have been
10 alphas and 1 beta before getting to this.

Why so long? Since that version, **all these changes have been implemented in my spare
time**.

One thing I did to improve
[this lack of funding](https://opensource.guide/getting-paid/) is.
[enable my sponsors page on Github](https://github.com/sponsors/yajo). Did it work?

- For one thing, I have earned a total of 0.01 € for each line of code added in all this
  time. Obviously, Copier will remain a hobby, because that's not enough for a living.
- On the other hand, I am touched that some users wanted to express their gratitude in
  this way, and **I appreciate it immensely**.

What is my reading of this situation?

- One of the best ways to show your gratitude for an open source project is with
  donations.
- When you open your next issue, think about this and be nice. Whoever made that program
  has put in a lot of hours so that you can use it. Take a few minutes to read the
  documentation, write a good issue, or open a good PR.
- If you have a company that uses free software, once a year stop to think:
  - Which open source projects are most important for my company?
  - How many hours of work do we save by using them?
  - How much would one of my employees get paid for each of those hours?
  - Then, multiply. You will know how much that software is worth to you.
  - Decide how much you want to donate to the maintainer. Maybe 10% of that? 1%?
    Whatever it is, it will help that software stay maintainable and your company keep
    saving money. Plus, you'll feel better for showing your gratitude, and so will the
    maintainer.

So I emphasize: **thank you so much to my fabulous donors!** 💌

Now let's change the subject and talk about the community. In this period there have
been 27 code contributors. Also, I opened
[the forum](https://github.com/copier-org/copier/discussions) and the community has been
able to converse there. I have been able to see how some members of the community solved
the doubts of others.

I can say that so far I am quite happy with how the Copier community is working.

Some tips from what I have learned as a maintainer:

- Close issues as soon as possible. The natural state of an issue is to be resolved. If
  it is not, it is exceptional.
- If it can't be resolved right now, have a clear triage process. Maybe put labels or
  milestones on them. Having the issues well organized is key to take advantage of the
  little time I can dedicate to the project.
- Always welcome contributors. If people feel comfortable, they will continue to
  contribute.
- Guide the contributors. A clear question or a short guiding comment accompanied by a
  good link is often more efficient than getting into a long debate about whether or not
  they have read all the documentation.
- Take advantage of contributor aids. Things like issue or PR templates help a lot.

Enjoy Copier 6!

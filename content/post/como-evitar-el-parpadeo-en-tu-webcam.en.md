---
title: How to avoid webcam flickering
draft: false
nocomments: false
date: 2021-12-21T18:52:57.222Z
tags:
  - webcam
  - windows
  - linux
  - udev
  - toolbx
  - logitech
---
Sometimes you may have a webcam that shows flickering.

How to solve it?

<!-- more -->

In my case, I have a *Logitech C920 HD Pro* and I was having this problem.

### Why it happens

Usually it is because the camera is working at 60 Hz and the monitor at 50, or vice versa.

## How to solve it in Windows

For my particular camera, it was simple:

1. download and install [the *Camera Settings* application](https://support.logi.com/hc/es/articles/360024849133--Descargas-HD-Pro-Webcam-C920).
2. Open it.
3. Set the camera's refresh rate to match that of the monitor.

## How to solve it on Linux

On Linux it is also easy to do if you install [the `gtk-v4l` package](https://pkgs.org/download/gtk-v4l). If you are on Fedora 35:

```shell
# Install the package
sudo dnf -y install gtk-v4l

# Run the app (you'll find it in the menu too)
gtk-v4l
```

It will open a graphical interface from which you can alter that parameter ("Power Line Frequency" in the picture) and many others:

![gtk-v4l UI](/assets/captura-de-pantalla-de-2021-12-21-18-46-18.png)

**The difficulty lies in making this change permanent**, since what you change here will be undone after your next restart.

To do this, you need to create a `udev` rule. In [this video](https://www.youtube.com/watch?v=DaZ9zU3tdFY) and [this blog](https://blog.christophersmart.com/2017/02/07/fixing-webcam-flicker-in-linux-with-udev/) they explain very well how to "cook" your rule, so I will post mine here:

```
SUBSYSTEM=="video4linux", \
SUBSYSTEMS=="usb", \
ATTRS{product}=="HD Pro Webcam C920", \
ATTRS{idProduct}=="082d", \
ATTRS{idVendor}=="046d", \
PROGRAM="/usr/bin/v4l2-ctl --device $devnode --set-ctrl power_line_frequency=1"
```

I saved it in `/etc/udev/rules.d/100-yajo-logitech-webcam.rules`. Obviously, for this we need to have `v4l2-ctl` installed, so for that:

```sh
sudo dnf install -y v4l-utils
systemctl reboot
```

After the reboot, your webcam will work as it should.
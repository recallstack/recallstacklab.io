---
title: How to take notes
draft: true
nocomments: false
date: 2021-05-14T07:00:45.725Z
tags:
  - notas
  - zettelkasten
  - vscode
  - syncthing
  - gitjournal
  - git
  - foam
  - markdown
  - markor
  - mark-text
  - sleek
  - todo.txt
  - zettelkasten
  - joplin
---
😯It seems like taking notes is something silly, but it's not that simple. My requirements:

1. **Sovereignty**. My notes should be mine. I want no corporations sneaking around them.
2. **Survival**. Notes apps keep changing... they get sold and bought, they expire, evolve... I'm not opposed to changing my app in the future, but I don't want to rewrite my notes.
3. **Sync**. I want to be able to write in my computer and read in my phone, and vice-versa. I use various operating systems.
4. **Offline**. If I have no Internet, I don't want to stay without them.

Extra points:

1. **Shareable**. If I want to write something in the grocery list, I want the rest of my family to see it too. And I want to see what they write.

   It's not a requirement because, for this specific use case, I can use a different app that allows sharing.
2. **Easy**. I'd like to be able to convince others to use my system. I'm a developer, but most of my friends are human beings.

   I don't consider it a requirement because I can write code, execute commands, set up servers... But it feels like an unnecessary distraction for the simple sake of taking notes.
3. **Alarms**. I'm very forgetful.

   It's not a requirement because I can use a different app for alarms.
4. **Formattable**. I can live with plain text, but... it's not what I like the most.
5. **Forgettable**. If I delete something, I'd like it to disappear forever.
6. **Handwritten**. If I can draw with a stylus in my tablet, it's an extra point.
7. **[Zettelkasten](https://zettelkasten.de/)**. Also known as second brain, or local wiki. It's an interesting learning method that tells you to write what you learn in small units (ideas) that are interlinked.

   It's not a requirement because these systems allow you to improve your knowledge, but not your wisdom, which is more important.

<!-- more -->

If we distill the requirements, we see that they are split between note capturing systems and sync systems, although it's possible to have both of them combined in the same app.

## For programmers 👨‍💻: GitJorunal + VSCode + Foam + Git

Ignoring some existing bugs, the experience was quite satisfactory. It has some good points:

* We use [git](http://git-scm.com/) to sync, with a virtually endless potential to organize and solve conflicts.
* We use [markdown](https://www.markdownguide.org/) to write down the notes, completely formattable.
* [GitJournal](https://gitjournal.io/) is quite ergonomic in mobile devices, and looks very nice:

  ![Different editors in GitJournal](https://camo.githubusercontent.com/7aed535cef2c5eabdb4bab1663e0bfddb089aab2cfedd68256f39d02ef991799/68747470733a2f2f6769746a6f75726e616c2e696f2f73637265656e73686f74732f616e64726f69642f323032302d30362d30342f656e2d47422f696d616765732f70686f6e6553637265656e73686f74732f4e657875732036502d352e706e67)

  ![Notes list in GitJournal](https://camo.githubusercontent.com/f08d789b3573b5e185ed6ed8f0c83c9550639ca081c0dc4362e486392eb61f61/68747470733a2f2f6769746a6f75726e616c2e696f2f73637265656e73686f74732f616e64726f69642f323032302d30362d30342f656e2d47422f696d616765732f70686f6e6553637265656e73686f74732f4e657875732036502d31332e706e67)
* [VSCode](https://code.visualstudio.com/) is comfortable and quick to write.
* [Foam](https://foambubble.github.io/foam/) allows to enhance somewhat the experience of taking notes and creating your Zettelkasten in VSCode, and [they recommend other extensions](https://foambubble.github.io/foam/recommended-extensions) that help in some particular aspects:

  ![VSCode with Foam](https://raw.githubusercontent.com/foambubble/foam/deb77328c0526c1d3cdbfad9fc8c444d3a59df23/docs/assets/images/foam-features-dark-mode-demo.png)

However, after awhile using them, I gave up. Reasons:

* The YAML front matter that GitJournal uses can become a distraction when using VSCode:

  ![Editing a note in GitJournal, where you can see the YAML front matter](https://camo.githubusercontent.com/3e9d1f44e8b134d617f46790a1dee24695fc25034ad524d7cb4f3adf959949a2/68747470733a2f2f6769746a6f75726e616c2e696f2f73637265656e73686f74732f616e64726f69642f323032302d30362d30342f656e2d47422f696d616765732f70686f6e6553637265656e73686f74732f4e657875732036502d362e706e67)
* VSCode has too many options available, that can become a distraction when taking notes.
* Making compatible both apps while keeping a minimally homogeneous experience takes its time (specially when configuring VSCode). Particularly, creating a new note in VSCode in a way directly usable in GitJournal is complicated.
* Having to solve some conflict in git is also distracting.
* Notes aren't encrypted. However, you can always create a git repository in your own server, which you only access via SSH, and in an encrypted disk. Practically speaking, it's a minor problem.
* Git won't allow forgetting. If you ever wrote something, it will stay in your history forever. Even if you delete notes, the used space will always increase. This point seems a bit critical to me. 

Probably, [a desktop version of GitJournal](https://github.com/GitJournal/GitJournal/issues/137) would simplify a lot this workflow, but it's not available for now.

Summarizing, to reduce distractions, disk space, time, and be able to forget what I want no more, I had to choose a different option.

## For semiprogrammers: Markor + Mark Text + Sleek + Syncthing

After desisting from the previous option, I tested a symilar system, but one that removes the need to use git and any app with too many distracting options, and I came to this combination. It has some advantages over the previous one:

* [Syncthing](https://syncthing.net/) is wonderful thing available on almost any platform, that allows us to synchronize any kind of file without too much effort, allowing us to save a customizable historic that allows to forget stuff, and with a conflict resolution that is less exhaustive than Git's (more comfortable for this use case). When you get used to it, you start using it for everything.

  ![Syncthing, displaying various directories being synced across various devices](https://raw.githubusercontent.com/syncthing/docs/9c2e90cba6c58f9de3a6caa50faaa5a7042248e5/intro/gui1.png)
* [Markor](https://gsantner.net/project/markor.html?source=github) is quite good to edit the [markdown](https://www.markdownguide.org/) and [todo.txt](http://todotxt.org/) formats in Android:

  ![Editing markdown in Markor](https://raw.githubusercontent.com/gsantner/markor/b69da230c89ec1419c973ced23356ca5931f343e/metadata/en-US/phoneScreenshots/03.jpg)

  ![Editing todo.txt in Markor](https://raw.githubusercontent.com/gsantner/markor/b69da230c89ec1419c973ced23356ca5931f343e/metadata/en-US/phoneScreenshots/05.jpg)
* On the desktop, [Mark Text](https://marktext.app/) and [Sleek](https://github.com/ransome1/sleek) are comfortable and beautiful when editing markdown and todo.txt respectively:

  ![Editing a markdown file in Mark Text](https://raw.githubusercontent.com/marktext/marktext/4dada5b84dfcf0546ed54f50b5cdb81fc9fc6a29/docs/themeImages/graphite-light.png)

  ![Editing a todo.txt in Sleek](https://raw.githubusercontent.com/ransome1/sleek/3e920fce6a789866f1d676dec2d01b807f689672/assets/screenshots/linux/todo_list.png)

Well, this system left me more satisfied than the previous one. However, it still had some disadvantages:

* That dance of changing between 4 different apps.
* Todo.txt seems excellent, but Markor has no alarms whatsoever, and Sleek doesn't allow sorting tasks without prioritizing them.
* Creating new notes wasn't very ergonomic in either case.
* Lack of encryption. As in the previous case, not a big problem.
* Still, too many distractions around.

Well, let's keep on searching...

## The winner: Joplin

I had already tested [Joplin](https://joplinapp.org/) previously, but I discarded it because I didn't like too much its aspect:

![Joplin, showing up its Android, terminal and desktop apps](https://raw.githubusercontent.com/laurent22/joplin/ebf92605aeabbd686d57952ab6b946d2db8dcf27/docs/images/AllClients.jpg)

Big mistake. I gave it another chance, investigating more deeply the options it has. It turned out it all my expectations were fulfilled more than reasonably:

* It uses markdown as internal format, but in desktop it allows to write using a visual editor that completely hides markdown code. Good! One less distraction.
* If you don't like any of its editors, you can use an external one.
* FLOSS, with a very active community.
* Organize notes in notebooks.
* A note can be converted into a task.
* Integrated sync, compatible with several back-ends (local file system, OneDrive, Dropbox, S3, WebDAV, or the own Joplin Server (the most optimized one that allows sharing notes)).
* It allows encrypting notes when syncing, so, even if you sync with a service that escapes your control, your notes are still private.
* It allows you to configure how many days of history to remember.
* It allows exporting and importing multiple formats, including a directory with markdown files.
* It allows attaching any kind of file to notes. It supports especially good videos, audios, images and PDF.
* It allows using an external editor for attachments, monitoring their changes.
* Apps for almost any OS.
* Lots of keyboard shortcuts and a notes and commands searcher similar to VSCode's.
* It allows setting up one alarm per note.
* Even its most advanced usage is simple.
* It really enhanced a lot my productivity.

Good for Joplin! I'm really enjoying the experience. It became one of those projects that make me feel enthusiastic. Actually, even some relative (human 😯) has already asked me to teach her use it!

However, it has its disadvantages:

* No support for todo.txt. With some creativity, you can organize your chores with similar criteria in markdown.
* Android editor has some errors and it's only plain text. Not very ergonomic. For now, I can handle it by just taking basic notes, and later when syncing in the PC I format them if necessary.
* [No support for handwritting](https://github.com/laurent22/joplin/issues/582), at least for now.
* To share notes among several users, you need to use Joplin Server back-end, which requires installing it and hosting it on some server.

## Other options available

There exist docens of other system I have tested and discarded. Obviously, my tests weren't very exhaustive given the huge amoutn of alternatives. Some of them were discarded for philosophical differences with the project, others for not being mature enough or not accomplishing my criteria.

Well, I put them here like a future reference, or just in case somebody is not convinced by any of the previous, sorted alphabetically:

* [Boost Note](https://boostnote.io/)
* [Carnet](https://getcarnet.app/)
* [Cherrytree](https://www.giuspen.com/cherrytree/)
* [Crossnote](https://crossnote.app/)
* [Evernote](https://evernote.com/)
* [GNOME Notes](https://wiki.gnome.org/Apps/Notes)
* [Google Keep](http://keep.google.com/)
* [Laverna](https://laverna.cc/)
* [Notable](https://notable.app/)
* [Notion](https://www.notion.so/)
* [QOwnNotes](https://www.qownnotes.org/)
* [RedNotebook](https://rednotebook.sourceforge.io/)
* [Samsung Notes](https://www.samsung.com/global/galaxy/apps/samsung-notes/)
* [Simplenote](https://simplenote.com/)
* [Standard Notes](https://standardnotes.org/)
* [TagSpaces](https://www.tagspaces.org/)
* [TiddlyRoam](https://tiddlyroam.org/)
* [Tomboy](https://github.com/tomboy-notes/tomboy-ng)
* [Trilium Notes](https://github.com/zadam/trilium)
* [Turtl](https://turtlapp.com/)
* [Unotes](https://github.com/ryanmcalister/unotes)
* [WizNote](https://www.wiz.cn/)
* [Zettlr](https://www.zettlr.com/)
* [Zim Wiki](https://zim-wiki.org/index.html)

Of course, if none convinces you, you can always use a notebook and a pen. 😃🗒️🖊️
---
title: Entendiendo los formatos de paquetes de software en Linux
draft: false
nocomments: false
date: 2022-02-06T15:44:15.268Z
tags:
  - linux
  - software
  - rpm
  - flatpak
  - deb
  - snap
  - appimage
---
> "¿Cómo instalar la aplicación \_\_\_\_\_\__?"

Seguro que te lo has preguntado. Si usas GNU/Linux, la respuesta habitual es: instalando un paquete de software.

Pero... es que existen muchos formatos de paquetes. ¿Cómo saber cuál es mejor?

Te cuento.

<!-- more -->

## Paquetes de sistema

Digo "de sistema" porque, al instalar un paquete en este formato, realmente lo que estamos haciendo es permitir **que un nuevo componente de software pase a formar parte de nuestro sistema operativo**.

Dependiendo del sistema operativo que uses, el paquete tendrá un formato diferente:

* `.deb` para Debian y derivados, como Ubuntu, Linux Mint...
* `.apk` para Alpine (no confundir con los `.apk` de Android).
* `.rpm` es el "estándar oficial", así que las distros que no lo usan nativamente suelen tener alguna forma de instalarlo, como por ejemplo a través de [alien](https://wiki.debian.org/Alien). Muchas otras lo usan como formato principal:

  * RHEL y derivados, como CentOS, Rocky Linux, Alma Linux, Oracle Linux...
  * Fedora
  * OpenSUSE
  * Mageia
  * etc.
* `.tar.gz`, `.tgz`, `.tbz2` y demás combinaciones que tengan una `t` y una `z` en algún sitio.  Esto son simplemente carpetas comprimidas que, al descomprimir en el lugar correcto, te dan la aplicación lista para usar.

Y hay más formatos y más familias de distros. [Ya se ha escrito mucho sobre el tema](https://es.wikipedia.org/wiki/Anexo:Distribuciones_Linux), así que no vale la pena enrollarme más.

En definitiva, **si vas a instalar la aplicación con un paquete de sistema, escoge el que más se acerque al que utilice tu distro de forma nativa**.

## Paquetes completos

Existe toda una gama de paquetes que *contienen la aplicación junto con todas sus dependencias* en su interior. La ventaja de estos paquetes es que **son más fáciles de instalar y funcionan en cualquier distro por igual**. La desventaja es que suelen ocupar más.

* [AppImage](https://appimage.org/). La idea es sencilla: un archivo que contiene cualquier programa junto con sus dependencias. Simplemente, lo descargas, lo colocas en la carpeta que te dé la gana, le das permisos de ejecución y lo ejecutas. Listo, tienes el programa funcionando en cualquier distro.\
  Lo bueno que tiene es que es sencillo de distribuir y de usar. Lo malo es que ocupa más, no ofrece integración con el escritorio ni la tienda de aplicaciones y tienes que actualizarlo a mano siguiendo el mismo proceso.\
  Existe [un catálogo bastante amplio de aplicaciones en formato AppImage](https://www.appimagehub.com/).
* [Snap](https://snapcraft.io/about). Parecido al anterior, pero con soporte para aplicaciones de servidor y [*una (única)* tienda de aplicaciones](https://snapcraft.io/). Exige unos puntos de montaje especiales y que tengas el cliente instalado y un servicio de sistema corriendo para usar las aplicaciones. Permite manejar actualizaciones y restringir permisos de las aplicaciones. Aunque se supone que un paquete snap funciona en cualquier distro, la realidad es que Ubuntu es el objetivo principal, y el resto son secundarias.
* [Flatpak](https://www.flatpak.org/). Con todos los puntos fuertes de Snap, pero sin sus puntos débiles: no necesita un servicio corriendo ni puntos de montaje especiales, permite activar diversos catálogos, empaquetar en diversas tecnologías, aprovechar mejor el espacio, actualizar la aplicación y el SDK por separado, tiene un mayor énfasis en el aislamiento por permisos de las aplicaciones, [un catálogo de aplicaciones principal](https://flathub.org/) (pero no único) y una mayor comunidad con más implicación de muchas distros.

De entre estos, **AppImage es el más sencillo** de usar de todos, pero tendrás que descargar las aplicaciones a mano y, como no tienen sistema de permisos, más te vale confiar ciegamente en el desarrollador. Además, tendrás que actualizarlas descargando la nueva versión a mano.

Snap y Flatpak son más completos, pero en muchos aspectos opino que **Flatpak es la mejor opción**. Eso sí, al tener más énfasis en el aislamiento por permisos, es posible que alguna aplicación no funcione bien del todo, sobre todo si no fue diseñada teniendo presente el sistema de permisos desde un mismo principio. Existe [Flatseal](https://flathub.org/apps/details/com.github.tchx84.Flatseal), que puedes instalar para gestionar los permisos de los flatpaks en estos casos.

## ¿Paquete de sistema, o paquete completo?

Ahora lo entiendes pero sigues con dudas, ¿no? Bien, te lo pondré sencillo.

Si puedes elegir, **elige paquete completo. Y, de entre ellos, Flatpak**.

## ¿Paquete oficial o comunitario?

Esto ya va a depender del programa.

A veces sucede que quien desarrolla un programa no es realmente muy experto en empaquetarlo, y por ello muchas veces funciona mejor el paquete comunitario que el oficial. Pero, por otro lado, si quieres apoyo técnico directamente de los desarrolladores, normalmente solo te lo darán si usas el paquete oficial.
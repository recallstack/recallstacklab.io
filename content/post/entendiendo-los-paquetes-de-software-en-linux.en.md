---
title: Understanding software package formats in Linux
draft: false
nocomments: false
date: 2022-02-06T15:44:15.268Z
tags:
  - linux
  - software
  - rpm
  - flatpak
  - deb
  - snap
  - appimage
---
> "How to install the application \_\_\_\_\_\__?"

I'm sure you've asked yourself. If you use GNU/Linux, the usual answer is: by installing a software package.

But... there are many package formats. How do you know which one is best?

Let me tell you.

<!-- more -->

## System packages

I say "system" because, by installing a package in this format, what we are really doing is allowing **a new software component to become part of our operating system**.

Depending on the operating system you use, the package may have a different format:

* `.deb` for Debian and derivatives, such as Ubuntu, Linux Mint....
* .apk for Alpine (not to be confused with Android `.apk`).
* `.rpm` is the "official standard", so distros that don't use it natively usually have some way to install it, such as via alien. Many others use it as their primary format:

  * RHEL and derivatives, such as CentOS, Rocky Linux, Alma Linux, Oracle Linux....
  * Fedora
  * OpenSUSE
  * Mageia
  * etc.
* `.tar.gz`, `.tgz`, `.tbz2` and other combinations that have a `t` and a `z` somewhere.  These are simply compressed folders that, when unzipped in the right place, give you the application ready to use.

And there are more formats and more families of distros. [Much has already been written on the subject](https://en.wikipedia.org/wiki/List_of_Linux_distributions), so it's not worth spending more time on that.

In short, **if you are going to install the application with a system package, choose the format closest to the one your distro uses natively**.

## Complete packages

There is a whole range of packages that *contain the application along with all its dependencies* inside. The advantage of these packages is that **they are easier to install and work on any distro equally**. The disadvantage is that they tend to take up more space.

* [AppImage](https://appimage.org/). The idea is simple: a file containing any program along with its dependencies. Simply, you download it, place it in the folder you want, give it execution permissions and run it. That's it, you have the program running on any distro.
  The good thing about it is that it is easy to distribute and use. The bad thing is that it takes up more space, it does not offer integration with the desktop or the application store and you have to update it by hand following the same process.
  There is [a fairly large catalog of applications in AppImage format](https://www.appimagehub.com/).
* [Snap](https://snapcraft.io/about). Similar to the previous one, but with support for server applications and [*one (unique)* application store](https://snapcraft.io/). Requires special mount points and that you have the client installed and a system service running to use the applications. It allows you to manage updates and restrict application permissions. Although a snap package is supposed to work on any distro, the reality is that Ubuntu is the primary target, and the rest are secondary.
* [Flatpak](https://www.flatpak.org/). With all the strengths of Snap, but without its weaknesses: it does not need a running service or special mount points, it allows to enable various catalogs, to package in various technologies, to make better use of space, to update the application and SDK separately, it has a greater emphasis on permission isolation of applications, [a main application catalog](https://flathub.org/) (but not unique) and a larger community with more involvement of many distros.

Of these, **AppImage is the easiest** to use of all, but you'll have to download the applications by hand and, since they have no permissions system, you'd better trust the developer blindly. Plus, you'll have to update them by downloading the new version by hand.

Snap and Flatpak are more complete, but in many ways I think **Flatpak is the best option**. However, since it has more emphasis on permission isolation, it is possible that some applications may not work completely well, especially if they were not designed with the permissions system in mind from the very beginning. There is [Flatseal](https://flathub.org/apps/details/com.github.tchx84.Flatseal), which you can install to manage flatpaks permissions in these cases.

## System package, or full package?

Now you understand, but you're still in doubt, aren't you? Well, I'll make it simple for you.

If you have a choice, **choose full package. And, among them, Flatpak**.

## Official or community package?

This will depend on the program.

Sometimes it happens that the developer of a program is not really an expert in packaging it, and therefore often the community package works better than the official one. But, on the other hand, if you want technical support directly from the developers, they will usually only give it to you if you use the official package.
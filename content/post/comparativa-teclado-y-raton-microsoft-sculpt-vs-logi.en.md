---
title: "Ergonomic keyboard and mouse comparison: Microsoft Sculpt vs. Logitech Ergo"
draft: true
nocomments: false
date: 2021-12-18T20:36:28.238Z
tags:
  - hardware
  - ergonomics
---
As the years go by, if you keep working with computers, you will gradually become more and more interested in ergonomics. Those "weird" ergonomic keyboards and mice... they're very expensive... Is it worth it?

I tell you my experience with 2 of them.

<!-- more -->

The first one, Microsoft Sculpt Ergonomic Desktop (keyboard and mouse in one package):

![Microsoft Sculpt Ergonomic Desktop](/assets/20211218_175016.jpg).

The second, the combination of the Logitech Ergo K860 keyboard and the Logitech Ergo MX Vertical mouse:

![Logitech Ergo K860 + MX Vertical](/assets/20211218_175141.jpg)

## Price

In both cases we are talking about sets that at the time of writing exceed 150€.

Are they worth their price? Well, let's see... I've had much less pain and tension in my wrists since I've been using them. For me, if you're going to be attached to them for long hours, **it's a very good investment**.

**Winner: Microsoft**. The Logitech ones must be bought separately, so it ends up being a bit more expensive. Personally I think the difference is not very significant, but it depends on your pocket.

## Connectivity

In the case of the Microsoft one, we are talking about a package that is purchased complete (although keyboard and mouse can be also sold separately).

At the bottom of the mouse there is a magnetic hole in which the **USB RF receiver** is hidden. Very tiny, and having it there is quite convenient.

That is, each combination of keyboard + mouse + numeric keypad has its own USB.

In the case of the Logitech products, both are products that connect via **Bluetooth**. Inside the box of each comes a Bluetooth receiver that connects via USB, the same size as the Microsoft receiver. But, if your device already has Blutetooth, you won't need to use it.

In addition, each of the devices has the ability to memorize **3 different connections**.

Obviously, **Logitech wins**. That thing of not having to occupy a USB with a receiver, coupled with the ability to memorize 3 different Bluetooth devices, gives a lot of possibilities. In addition, it is much easier to use it for the tablet or mobile without the need for an OTG cable.

That said, **if for example you intend to "wake up" your suspended PC** by pressing a key, or if you need to type a decryption key before the operating system loads, **a Bluetooth keyboard won't work** for that, since when the PC is turned off, they are disconnected; while a USB keyboard will work even with the PC in one of those modes. If your PC is a laptop, which already includes its own keyboard and trackpad, it shouldn't be much of a problem.

## Function keys

Microsoft: toggled with a switch.

Logitech: activated by pressing the <kbd>Fn</kbd> key in combination with the key you want to activate. You can also use <kbd>Fn</kbd>+<kbd>🔒</kbd> to toggle whether the default press is the special or normal function.

**Winner: Logitech**, hands down.

## Lift

For certain positions, such as if you're working standing up, or sitting down on a pilates ball, it's often more ergonomic to have the keyboard higher on the wrist. Both keyboards allow this.

Microsoft: There is an included separate add-on (shown in the photo), magnetic. It sticks to the keyboard and raises the wrist.

Logitech: It has tabs underneath the keyboard, which allow it to be raised or not.

**Winner: Logitech**, since you don't need to have 2 pieces for the same purpose. Although it must be said that the Microsoft add-on looks more robust.

## Extra mouse keys

Both the Microsoft and the Logitech have a key on top, to press it with the thumb, which is quite uncomfortable and I don't know what it is for because, at least in Linux, it does nothing.

Also both have, in a more comfortable position for the thumb a back button, very handy when you're surfing the internet.

However, there is an important difference: while **the Microsoft mouse allows you to scroll horizontally** when tilting the wheel sideways, **the Logitech one has another key** on the thumb to **go forward**, as much practical as the back button.

Winner: in this case I'd say we're in a **draw**, as both have something the other doesn't.

## Key layout

Both keyboards have a special shape in the center that will force your hands to be in a more natural position. However, **if you don't know typing, it's going to take some time getting used to them**. Maybe that's a good reason to learn it? It's not that complicated, and I can tell you that it's one of the skills I've gotten the most out of in my life.

Aside from that, the Microsoft keyboard has certain keys in more "creative" positions that make it more unnatural if you're coming from a traditional keyboard.

The Logitech has all the keys where you would expect to find them.

**Winner: Logitech** I don't think it's the most important thing though; after a while, you end up getting used to the Microsoft.

## Presence

**Winner: Logitech**. Microsoft's keyboard looks like it was designed for the Batcave. Although I guess it depends on your taste....

## Durability

Regarding the Logitech, I can't give my opinion because I have it for a very short time. The materials look of very good quality, though.

However, **the Microsoft combo has a very bad durability**, especially considering that we are talking about a high-end equipment. I've had to replace it about 3 times now. Fortunately, something always went wrong before 2 years and it was under warranty, but you have to be patient to fight with Microsoft technical support and convince them that the device doesn't work. I had my numeric keypad broken once, and my mouse twice.

Winner: I have nothing to compare it to, but I can say that the poor durability of the Microsoft one is the main reason I wanted to try the Logitech one.

## Ergonomics

I'll leave what is perhaps the most interesting point for last, because if there's one thing that really justifies these devices, it's ergonomics.

As for the keyboards, I think the ergonomics are almost identical. I think the Microsoft wins a little bit because, by having the numeric keypad separately, it allows you to put the mouse between the normal keyboard and the numeric keypad, so that the right arm is not so wide open.

However, that also makes the position of the keys more awkward, and it's not really something you miss when you jump to the Logitech.

Regarding the mouse, both are very comfortable, but the Logitech feels more ergonomic by far. Besides, it doesn't have a laser that hurts your eyes when you turn it around.

Winner: **the Microsoft keyboard narrowly wins, and the Logitech mouse wins by far**.

## Final verdict

Winner: both options win and lose at different points. Putting everything on balance, for me **the Logitech equipment is the winner**.
---
title: Wobbly windows in GNOME
date: 2020-04-25T12:11:52Z
tags:
  - Compiz
  - Gnome
---

Something completely unnecessary but essential at the same time:

{{< figure src="/assets/gnome-wobbly-windows.gif" class="img img-thumbnail" >}}

To install it:

<!-- more -->

- [Page to install the extension](https://extensions.gnome.org/extension/2950/compiz-alike-windows-effect/).
- [Source code](https://github.com/hermes83/compiz-alike-windows-effect).

It doesn't work as good as the old Compiz, but it's better than nothing!

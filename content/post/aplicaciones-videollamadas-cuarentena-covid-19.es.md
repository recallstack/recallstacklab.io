---
title: Aplicaciones para hacer videollamadas durante tu cuarentena del COVID-19
draft: false
date: 2020-03-19T16:21:31.000Z
tags:
  - google duo
  - jitsi meet
  - skype
  - facetime
  - zoom
  - whatsapp
nocomments: false
---

Medio planeta en cuarentena, y el otro medio lo estará dentro de poco (aunque aún no lo
crea)... ¡pero queremos mantenernos en contacto con nuestros conocidos! ¿Qué opciones
tenemos?

<!--more-->

## Videollamadas de WhatsApp

La opción de Facebook (recordemos que es el dueño de WhatsApp). ¿Quién no tiene WhatsApp
hoy en día? (Afortunado eres...)

{{< figure src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/WhatsApp_screenshot_rus.png/300px-WhatsApp_screenshot_rus.png" title="WhatsApp" link="https://www.whatsapp.com/" attr="Cortesía de Anslem Douglas y Wikimedia" attrlink="https://commons.wikimedia.org/wiki/File:WhatsApp_screenshot_rus.png" class="img-thumbnail" >}}

Ventajas:

- Alta cuota de mercado. O sea, casi el 100%.
- Se ve y oye bien.

Inconvenientes:

- Código propietario.
- Llamadas grupales de hasta máximo 4 personas.
- Necesitas instalar la aplicación en tu móvil.
- Necesitas tener una cuenta, dando datos personales críticos como tu número de teléfono
  real y permitiéndole acceso a Facebook a toda tu libreta de contactos.
- No es multidispositivo. Vamos, que tienes que hacer la llamada en la minipantalla de
  tu móvil.
- No es multiplataforma. Solo Android e iOS.
- No permite ver solo a quien habla. Siempre la pantalla se divide en 4. Y recordemos
  que es la minipantalla de tu móvil.

Notas extra:

- [Instrucciones para las videollamadas](https://faq.whatsapp.com/en/android/26000026/?category=5245237&lang=es).

## Skype

La opción de Microsoft. Muy conocido, lleva mucho tiempo entre nosotros.

{{< figure src="/assets/skype.png" title="Skype" link="https://www.skype.com/" class="img-thumbnail" >}}

Ventajas:

- Buena calidad de vídeo.
- Buena resiliencia ante redes con mucha latencia.
- Hasta 50 participantes gratis.
- Multiplataforma.
- Permite iniciar llamadas.

Inconvenientes:

- Código propietario.
- Difícil de usar.
- Necesitas instalar la aplicación.
- Necesitas tener una cuenta Microsoft.
- No permite entrar en una reunión via enlace (que yo sepa).
- Cancelación de eco ineficiente. Quiero decir: en lugar de filtrar de la entrada de
  audio el sonido que sale por los altavoces, lo que hace es silenciar todos los
  micrófonos salvo el que suene más fuerte. Esto produce que cuando alguien te susurra
  el micrófono se le cierre, o que cuando alguien te responde antes de que hayas
  terminado de hablar (lo que es de mala educación, por otro lado), ambos pierden lo que
  dice el otro.

Notas extra:

- [En Linux puedes instalar Skype desde FlatHub][skype-flathub]; es mejor que desde la
  web oficial.

## Zoom

Especialmente diseñado para reuniones muuuy grandes, a nivel profesional.

{{< figure src="/assets/zoom.png" title="Zoom" link="https://zoom.us/" class="img-thumbnail" >}}

Ventajas:

- Compartir pantalla es una experiencia exquisita:
  - Te coloca los controles arriba de la pantalla cómodamente escondidos.
  - Permite ver una miniatura de la reunión y colocarla donde quieras.
  - En el modo normal prima la calidad de imagen, pero al compartir en "modo vídeo",
    bajará la calidad dando prioridad a la fluidez de la reproducción, automáticamente.
  - Permite compartir solo una ventana o solo un escritorio, incluso desde Linux.
  - Permite compartir el escritorio en Android e iOS.
  - Permite definir si compartes o no el audio del escritorio.
- En su mejor versión llega a los 1000 participantes por reunión.
- Permite iniciar llamadas.
- Permite unirse a una reunión.
- Permite controles estrictos de permisos en una reunión:
  - Quién es anfitrión (puede haber varios).
  - Quién puede hablar.
  - Expulsar asistentes.

Inconvenientes:

- Código propietario.
- La versión gratuita solo permite reuniones grupales de hasta 40 minutos.
- Necesitas instalar la aplicación.
- Necesitas tener una cuenta.

Notas extra:

- [En Linux puedes instalar Zoom desde FlatHub][zoom-flathub]; es mejor que desde la web
  oficial,
  [aunque por ahora no funcionará compartir pantalla con Wayland](https://github.com/flathub/us.zoom.Zoom/issues/22).

## FaceTime

Para los manzaneros.

{{< figure src="https://live.staticflickr.com/7248/7857087992_f48f2e3ba9.jpg" title="FaceTime" link="https://apps.apple.com/es/app/facetime/id1110145091" class="img-thumbnail" >}}

Ventajas:

- Sencillo de usar.
- Puedes ponerte una jirafa en la cabeza.
- Permite llamar.

Inconvenientes:

- Código propietario.
- Solo llamadas de 1 a 1.
- Solo en dispositivos Apple.
- No permite unirse a una reunión.

## Google Duo

Para no usar más el teléfono. De mis favoritos. 😍

{{< figure src="https://duo.google.com/about/static/images/be-together.png" title="Google Duo" link="https://duo.google.com/" class="img-thumbnail" >}}

Ventajas:

- Fácil de usar.
- Permite hacer llamadas.
- Se vincula con tu libreta de contactos para encontrar gente que use Duo, usando tu
  número de teléfono. Vamos, como WhatsApp.
- Además de videollamadas, permite dejar mensajes de vídeo cuando no te pueden atender.
- Multiplataforma.
- En escritorio, no es necesario instalar nada.
- Encriptación de punto a punto.
- Llamadas grupales de hasta 12 participantes.
- Gratis.

Desventajas:

- Código propietario.
- Necesitas tener una cuenta, dando datos personales críticos como tu número de teléfono
  real y permitiéndole acceso a Google a toda tu libreta de contactos.
- No tiene herramientas de moderación en las llamadas grupales.
- No permite compartir escritorio.

## Jitsi Meet

Mi favorito. 😍

{{< figure src="https://jitsi.org/wp-content/uploads/2017/06/jitsi-front.png" title="Jitsi Meet" link="https://meet.jit.si/" class="img-thumbnail" >}}

Ventajas:

- [Código abierto](https://github.com/jitsi/jitsi-meet).
- [Sin límite de participantes][jitsi-limit], siempre y cuando los clientes lo soporten.
- 100% gratis.
- Sencillísimo.
- Permite unirse a una reunión.
- No necesitas tener una cuenta.
- No necesitas instalar una aplicación (aunque en móviles y tabletas es mejor
  instalarla).
- Permite unirse por teléfono, con puentes telefónicos en diversos países, entre ellos
  un fijo de España.

Inconvenientes:

- No Permite iniciar llamadas, sino que tienes que compartir el enlace de la reunión con
  el destinatario por otro medio cualquiera, y esperar a que él se conecte.
- Funciona bien con cualquier navegador, pero por ahora soportan oficialmente solo
  Google Chrome/Chromium.
- Puede que en un ordenador lento la experiencia web no llegue al rendimiento de una
  aplicación nativa.

[skype-flathub]: https://www.flathub.org/apps/details/com.skype.Client
[zoom-flathub]: https://www.flathub.org/apps/details/us.zoom.Zoom
[jitsi-limit]: https://community.jitsi.org/t/participants-can-join-per-room/18983/4

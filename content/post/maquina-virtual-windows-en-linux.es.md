---
title: Cómo crear una máquina virtual Windows dentro de Linux
date: 2020-10-02T20:24:20Z
tags:
  - GNOME Boxes
  - Linux
  - SPICE
  - Virtualización
  - Windows
  - Wine
---

Tener una máquina virtual Windows en Linux tiene varias utilidades:

1. Si estás probando Linux por primera vez, te puede servir para "desengancharte" poco a
   poco. Sabes que tienes Windows ahí, y ese placebo te da el valor necesario para
   probar este nuevo y extraño mundillo que es Linux.
1. Eres desarrollador y quieres ver cómo funcionan tus aplicaciones o servicios en
   Windows.
1. Necesitas usar una aplicación para la que no hay alternativa nativa en Linux y que no
   puedes correr con [Wine](https://www.winehq.org/).
1. Necesitas usar una aplicación que solo está disponible en
   [la tienda de aplicaciones de Windows](https://www.microsoft.com/store/apps/windows).

Pues resulta que conseguirla es mucho más sencillo de lo que te imaginas:

<!-- more -->

1.  **Instala un gestor de máquinas virtuales.**

    Yo voy a usar "Cajas". Tendrás que instalarlo usando el gestor de paquetes de tu
    distribución. El paquete casi seguro que se llamará `gnome-boxes`.

    Atención: no lo instales desde [Flathub](https://flathub.org/) porque tiene todavía
    [un problema en la gestión de red](https://gitlab.gnome.org/GNOME/gnome-boxes/-/issues/232)).

    Para mí, en Fedora Silverblue, el comando a correr es este, que además me reiniciará
    el sistema tras instalar:

    ```bash
    rpm-ostree install --reboot gnome-boxes
    ```

    Si usas otra distro que no esté basada en
    [OSTree](https://ostree.readthedocs.io/en/latest/), probablemente no necesites
    reiniciar. De hecho, será tan sencillo como buscar "Cajas" en la tienda de
    aplicaciones e instalarla:

    {{< figure src="/assets/gnome-boxes-install-in-ubuntu-20-04.gif" caption="Instalando Cajas en Ubuntu 20.04" >}}

    En [pkgs.org](https://pkgs.org/search/) puedes obtener más detalles sobre
    [dónde encontrar ese paquete en cada distro](https://pkgs.org/search/?q=gnome-boxes).

1.  **Descarga la máquina virtual de Windows.**

    Para ello, visita uno de estos, dependiendo de si quieres un entorno de desarrollo
    completo, o solo vas a usarla para instalar algún que otro programa:

    - [Entorno de desarrollo completo de Windows 10](https://developer.microsoft.com/windows/downloads/virtual-machines/)
      (unos 20 GiB).
    - [Windows 10 con MSEdge](https://developer.microsoft.com/microsoft-edge/tools/vms/)
      (unos 8 GiB).

    Descarga la versión para VirtualBox más reciente disponible.

    Incluyen una licencia de Windows válida durante 90 días, así que tendrás que repetir
    el proceso de vez en cuando.

1.  **Descomprímelo:**

    {{< figure src="/assets/windows-vm-unzip.gif" caption="Descomprimiendo la máquina virtual Windows" >}}

1.  **Abre "Cajas" y crea una nueva caja usando ese archivo:**

    {{< figure src="/assets/windows-vm-create.gif" caption="Creando la máquina virtual Windows" >}}

    Nota: Windows solito ya consumirá unos 3 GiB de memoria, así que asígnale como
    mínimo unos 6 para que las aplicaciones que abras puedan respirar un poco.

1.  **Configura el idioma y teclado dentro de la caja**, por comodidad:

    {{< figure src="/assets/windows-vm-spanish.gif" caption="Configurando la disposición del teclado y la configuración regional en la máquina virtual Windows" >}}

1.  **Instala [las extensiones SPICE](https://www.spice-space.org/download.html) en
    Windows** para que se integre mejor con tu máquina anfitriona. Permite cosas como
    detectar la resolución de pantalla, compartir el portapapeles o evitar tener que
    pulsar <kbd>Ctrl</kbd>+<kbd>Alt</kbd> para soltar el ratón:

    {{< figure src="/assets/windows-vm-spice-guest-tools-installation.gif" caption="Instalando las extensiones SPICE en la máquina virtual Windows" >}}

¡Listo! Ya tienes un Windows para ponerte ahí lo que quieras.

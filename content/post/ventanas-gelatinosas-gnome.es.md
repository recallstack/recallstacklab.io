---
title: Ventanas gelatinosas en GNOME
date: 2020-04-25T12:11:52Z
tags:
  - Compiz
  - Gnome
---

Algo completamente innecesario pero imprescindible al mismo tiempo:

{{< figure src="/assets/gnome-wobbly-windows.gif" class="img img-thumbnail" >}}

Para instalarlo:

<!-- more -->

- [Página para instalar la extensión](https://extensions.gnome.org/extension/2950/compiz-alike-windows-effect/).
- [Código fuente](https://github.com/hermes83/compiz-alike-windows-effect).

No funciona tan bien como el viejo Compiz, ¡pero es mejor que nada!

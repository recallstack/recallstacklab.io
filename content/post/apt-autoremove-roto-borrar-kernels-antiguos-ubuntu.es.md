---
title: Cómo borrar kernels antiguos en Ubuntu cuando apt-get autoremove se rompe
date: 2020-08-31T21:06:57Z
tags:
  - apt-get
  - Linux
  - Ubuntu
---

¿Qué hacer cuando te encuentras esta 💩 en la terminal?

```bash
➤ sudo apt-get remove linux-image-generic
Leyendo lista de paquetes... Hecho
Creando árbol de dependencias
Leyendo la información de estado... Hecho
Tal vez quiera ejecutar «apt-get -f install» para corregirlo:
Los siguientes paquetes tienen dependencias incumplidas:
 linux-generic : Depende: linux-image-generic (= 4.4.0.143.151) pero no va a instalarse
 linux-modules-extra-4.4.0-143-generic : Depende: linux-image-4.4.0-143-generic pero no va a instalarse o
                                                  linux-image-unsigned-4.4.0-143-generic pero no va a instalarse
 linux-signed-image-generic : Depende: linux-image-generic pero no va a instalarse
E: Dependencias incumplidas. Intente «apt-get -f install» sin paquetes (o especifique una solución).
```

Complicadete, pero vamos a ello...

<!-- more -->

## ¿Qué está pasando?

En seguida lo vas a saber si ejecutas esto:

<pre>
➤ sudo df -h | grep /boot
/dev/md126p2                              473M   473M     0 <b>100% /boot</b>
/dev/md126p1                              513M   3,4M  509M   1% /boot/efi
</pre>

¿Observas ese `100% /boot` tan simpático? Te está diciendo que no te caben más kernels
en la partición de arranque.

Ubuntu es una `<sarcasm>`maravilla`</sarcasm>` que te permite acumular kernels hasta el
infinito. Además, el tamaño de la partición `/boot` que te recomienda por defecto es
bastante pequeño. Y además, cuando se te llena, no puedes ejecutar
`sudo apt-get autoremove` para borrar kernels antiguos porque, como no puede construir
un árbol de dependencias correcto, da error antes de poder llegar a borrar nada.

## La solución

Vamos por pasos:

### 1. Liberar espacio en `/boot`

Para ello, primero que nada, necesitas saber qué kernel estás usando:

```bash
➤ uname -r
4.4.0-142-generic
```

Ahora que tienes eso claro, recuerda: **no borres ese kernel ni ninguno de una versión
superior**.

Ahora vamos a ver qué está llenando `/boot`:

```bash
➤ ls -lh /boot/initrd.img-*
-rw-r--r-- 1 root root 9,3M dic 15  2018 /boot/initrd.img-4.4.0-109-generic
-rw-r--r-- 1 root root  39M dic 15  2018 /boot/initrd.img-4.4.0-130-generic
-rw-r--r-- 1 root root  39M dic 15  2018 /boot/initrd.img-4.4.0-133-generic
-rw-r--r-- 1 root root  39M dic 15  2018 /boot/initrd.img-4.4.0-134-generic
-rw-r--r-- 1 root root  39M dic 15  2018 /boot/initrd.img-4.4.0-137-generic
-rw-r--r-- 1 root root  39M dic 15  2018 /boot/initrd.img-4.4.0-138-generic
-rw-r--r-- 1 root root  39M dic 15  2018 /boot/initrd.img-4.4.0-139-generic
-rw-r--r-- 1 root root  39M ene 12  2019 /boot/initrd.img-4.4.0-141-generic
-rw-r--r-- 1 root root  39M feb  7  2019 /boot/initrd.img-4.4.0-142-generic
```

En mi caso, puedo borrar todos salvo el último (que es el que estoy usando), pero en
realidad con borrar 3 ó 4 seguramente tenga suficiente espacio libre. Vamos a ello. ⚠
Cuidadito, que es una operación destructiva:

```bash
➤ rm /boot/initrd.img-4.4.0-109-generic /boot/initrd.img-4.4.0-130-generic /boot/initrd.img-4.4.0-133-generic /boot/initrd.img-4.4.0-134-generic
```

Vemos que ya hay espacio libre:

<pre>
➤ sudo df -h | grep /boot
/dev/md126p2                              473M   349M  100M  <b>78% /boot</b>
/dev/md126p1                              513M   3,4M  509M   1% /boot/efi
</pre>

### 2. Reconstruir el índice de apt

Ahora, para que [apt](https://es.wikipedia.org/wiki/Advanced_Packaging_Tool) deje de dar
la lata, tenemos que seguir la sugerencia que el propio apt nos estaba dando en el
primer mensaje:

```bash
➤ sudo apt-get -f installe
```

Esto instalará las nuevas dependencias y reconstruirá el menú de arranque. Si ha
fallado, la solución es borrar algún `/boot/initrd.img-*` más, para liberar más espacio.

Si ha funcionado, comprobemos que apt vuelve a ser feliz:

```bash
➤ sudo apt-get check
Leyendo lista de paquetes... Hecho
Creando árbol de dependencias
Leyendo la información de estado... Hecho
```

### 3. Borrar los kernels viejos, esta vez usando apt

Efectivamente, ahora apt se piensa que existen unos archivos que no existen porque los
hemos borrado a lo cowboy 🤠, así que vamos a decirle a apt que nos borre los kernels
viejos de forma oficial 👮:

```bash
➤ sudo apt-get autoremove --purge
Leyendo lista de paquetes... Hecho
Creando árbol de dependencias
Leyendo la información de estado... Hecho
Los siguientes paquetes se ELIMINARÁN:
  linux-headers-4.4.0-130* linux-headers-4.4.0-130-generic* linux-headers-4.4.0-133* linux-headers-4.4.0-133-generic* linux-headers-4.4.0-134* linux-headers-4.4.0-134-generic* linux-headers-4.4.0-137* linux-headers-4.4.0-137-generic*
  linux-headers-4.4.0-138* linux-headers-4.4.0-138-generic* linux-headers-4.4.0-139* linux-headers-4.4.0-139-generic* linux-headers-4.4.0-141* linux-headers-4.4.0-141-generic* linux-image-4.4.0-130-generic* linux-image-4.4.0-133-generic*
  linux-image-4.4.0-134-generic* linux-image-4.4.0-137-generic* linux-image-4.4.0-138-generic* linux-image-4.4.0-139-generic* linux-image-4.4.0-141-generic* linux-image-extra-4.4.0-130-generic* linux-image-extra-4.4.0-133-generic*
  linux-image-extra-4.4.0-134-generic* linux-image-extra-4.4.0-137-generic* linux-image-extra-4.4.0-138-generic* linux-image-extra-4.4.0-139-generic* linux-image-extra-4.4.0-141-generic* linux-signed-image-4.4.0-130-generic*
  linux-signed-image-4.4.0-133-generic* linux-signed-image-4.4.0-134-generic* linux-signed-image-4.4.0-137-generic* linux-signed-image-4.4.0-138-generic* linux-signed-image-4.4.0-139-generic* linux-signed-image-4.4.0-141-generic*
0 actualizados, 0 nuevos se instalarán, 35 para eliminar y 73 no actualizados.
Se liberarán 2.119 MB después de esta operación.
¿Desea continuar? [S/n]
```

Si estamos de acuerdo con esa lista (que por lo visto son todos los kernels viejos),
pulsamos <kbd>Enter</kbd> y apt hará el resto.

Reinicia si quieres, para usar algún nuevo kernel que se te haya instalado.

¡Listo!

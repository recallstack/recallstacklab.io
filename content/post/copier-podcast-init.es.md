---
title: Copier en Podcast.__init__
date: 2021-01-12T17:33:18Z
tags:
  - copier
---

Me han entrevistado en [Podcast.\_\_init\_\_](https://www.pythonpodcast.com/) hablando
acerca de [Copier](https://copier.readthedocs.io/en/stable/).

<!-- more -->

[Escuchar entrevista](https://www.pythonpodcast.com/copier-project-scaffolding-episode-297/).

---
title: Copier on Podcast.__init__
date: 2021-01-12T17:33:18Z
tags:
  - copier
---

I've been interviewed in [Podcast.\_\_init\_\_](https://www.pythonpodcast.com/) talking
about [Copier](https://copier.readthedocs.io/en/stable/).

<!-- more -->

[Listen to the interview](https://www.pythonpodcast.com/copier-project-scaffolding-episode-297/).

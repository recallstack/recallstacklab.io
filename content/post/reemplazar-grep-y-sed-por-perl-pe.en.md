---
title: Replace grep and sed by perl -pe
draft: false
nocomments: false
date: Su/03/yyyyT09:24
tags:
  - linux
  - regex
---
Did you know that you can replace `grep` and `sed` by `perl -pe` and have the full power of Perl regexs?

Examples:

<!-- more -->

Imagine you have a file with comma-separated Odoo module names and you want to extract all those that:

1. Are unrelated to `payment`.
2. Are from any localization except the Spanish one.

Using only `grep` or `sed` is very complicated because they don't support [negative lookaheads](https://regextutorial.org/positive-and-negative-lookahead-assertions.php). However, with Perl it's "very" easy:

```
$ cat example.txt
account,account_payment,l10n_es,l10n_es_reports,l10n_mx,l10n_mx_reports,l10n_se,l10n_se_reports,payment

$ perl -pe "s/,?\w*(l10n_(?!es_?)|payment)\w*//g" example.txt
account,l10n_es,l10n_es_reports
```

---
title: "Comparativa teclado y ratón ergonómicos: Microsoft Sculpt vs. Logitech Ergo"
draft: false
nocomments: false
date: 2021-12-18T20:36:28.238Z
tags:
  - hardware
  - ergonomía
---
Conforme pasan años, si sigues trabajando en informática, poco a poco te va interesando más la ergonomía. Esos teclados y ratones ergonómicos tan "raros"... son muy caros... ¿vale la pena?

Te cuento mi experiencia con 2 de ellos.

<!-- more -->

El primero de ellos, Microsoft Sculpt Ergonomic Desktop (teclado y ratón en un solo paquete):

![Microsoft Sculpt Ergonomic Desktop](/assets/20211218_175016.jpg)

El segundo, la combinación del teclado Logitech Ergo K860 y el ratón Logitech Ergo MX Vertical:

![Logitech Ergo K860 + MX Vertical](/assets/20211218_175141.jpg)

## Precio

En ambos casos estamos hablando de conjuntos que a fecha de escritura sobrepasan los 150€.

¿Valen su precio? Pues a ver... yo mucho menos dolor y tensión en las muñecas desde que los uso. Para mí, si vas a estar muchas horas pegado a ellos, **es una inversión muy buena**.

**Ganador: Microsoft**. El Logitech tienes que comprarlo por separado obligatoriamente, así que acaba saliendo algo más caro. Personalmente creo que la diferencia no es muy significativa, pero depende del bolsillo.

## Conectividad

En el caso del Microsoft, estamos hablando de un paquete que se compra completo (aunque también se venden teclado y ratón por separado).

En la parte inferior del ratón hay un hueco magnético en el cual se esconde el **receptor de radiofrecuencia USB**. Muy pequeñito, y tenerlo ahí es bastante cómodo.

Es decir, cada combinación de teclado + ratón + teclado numérico tiene su propio USB.

En el caso de los productos Logitech, ambos son productos que se conectan por **Bluetooth**. Dentro de la caja de cada uno, viene un receptor Bluetooth que se conecta por USB, del mismo tamaño que el receptor Microsoft. Pero, si tu dispositivo ya tiene Blutetooth, no necesitarás usarlo.

Además, cada uno de los dispositivos tiene capacidad de memorizar **3 conexiones** diferentes.

Obviamente, **Logitech gana**. Eso de no tener que ocupar un USB con un receptor, sumado a la posibilidad de memorizar 3 dispositivos Bluetooth diferentes, da muchas posibilidades. Además, es mucho más fácil usarlo para la tablet o móvil sin necesidad de un cable OTG.

Eso sí, **si por ejemplo pretendes "despertar" tu PC suspendido** pulsando una tecla, o si necesitas escribir una clave de desencriptado antes de que se cargue el sistema operativo, **un teclado Bluetooth no te sirve** para eso, ya que cuando se apaga el PC, se desconectan; mientras que un teclado USB sí funcionará incluso con el PC en uno de esos modos. Si tu PC es un portátil, que incluye ya su propio teclado y trackpad, no debería ser un gran problema.

## Teclas de función

Microsoft: se activan o desactivan con un interruptor.

Logitech: se activan pulsando la tecla <kbd>Fn</kbd> en combinación con la tecla que desees activar. También puedes usar <kbd>Fn</kbd>+<kbd>🔒</kbd> para conmutar si la pulsación por defecto es la función especial o la normal.

**Ganador: Logitech**, clarísimamente.

## Elevador

Para ciertas posiciones, como si trabajas de pié o sentado en una bola de pilates, suele ser más ergonómico tener el teclado más elevado en la muñeca. Ambos teclados lo permiten.

Microsoft: Se incluye un complemento aparte (se ve en la foto), magnético. Se pega al teclado y eleva la muñeca.

Logitech: Tiene unas pestañitas abajo del teclado, que permiten elevarse o no.

**Ganador: Logitech**, ya que no necesitas tener 2 piezas para el mismo cometido. Aunque hay que decir que el complemento del Microsoft se ve más robusto.

## Teclas extra del ratón

Tanto el Microsoft como el Logitech tienen una tecla arriba, para pulsarla con el pulgar, que queda bastante incómoda y que no sé para qué sirve porque, al menos en Linux, no hace nada.

También ambos tienen, en una posición más cómoda para el pulgar un botón de retroceder, muy práctico cuando estás navegando por internet.

No obstante, hay una diferencia importante: mientras que **el ratón de Microsoft permite hacer scroll horizontal** con la rueda inclinándola lateralmente, **el de Logitech tiene otra tecla** en el pulgar que sirve **para avanzar**, tan práctico como el de retroceder.

Ganador: en este caso diría que estamos en un **empate**, ya que ambos tienen algo que el otro no.

## Disposición de teclas

Ambos teclados tienen una forma especial en el centro que obligará a tus manos a estar en una posición más natural. Eso sí, **si no sabes mecanografía, te va a costar acostumbrarte**. ¿Quizás es una buena razón para aprenderla? No es tan complicado, y puedo decirte que es una de las habilidades que más he amortizado en mi vida.

Aparte de ello, el teclado Microsoft tiene ciertas teclas en posiciones más "creativas" que lo hacen más antinatural si vienes de un teclado tradicional.

El Logitech tiene todas las teclas donde esperarías encontrarlas.

**Ganador: Logitech.** Aunque no creo que sea lo más importante; al cabo de un tiempo, terminas acostumbrándote al Microsoft.

## Presencia

**Ganador: Logitech**. El teclado de Microsoft parece diseñado para la Batcueva. Aunque supongo que va a gustos...

## Durabilidad

Respecto al Logitech, no puedo opinar porque lo tengo muy poco tiempo. Los materiales se ven de muy buena calidad, eso sí.

No obstante, **el combo de Microsoft tiene una durabilidad malísima**, máxime teniendo presente que hablamos de un equipo de gama alta. He tenido que cambiarlo ya como 3 veces. Afortunadamente, siempre se estropeaba algo antes de 2 años y entraba en garantía, pero tienes que armarte de paciencia para pelearte con el servicio técnico de Microsoft y convencerles de que el aparato no funciona. Una vez se me estropeó el teclado numérico, y 2 veces el ratón.

Ganador: no tengo con qué comparar, pero puedo afirmar que la mala durabilidad del Microsoft es la razón principal que tuve para querer probar el Logitech.

## Ergonomía

Me dejo el que quizás sea el punto más interesante para el final, ya que si hay algo que realmente justifica estos dispositivos es la ergonomía.

En cuanto a los teclados, creo que la ergonomía es casi idéntica. Creo que el Microsoft gana un poquito porque, al tener el teclado numérico por separado, permite poner el ratón entre el teclado normal y el numérico, de forma que el brazo derecho no queda tan abierto.

No obstante, eso también hace que la posición de las teclas sea más extraña, y realmente no es algo que se eche en falta cuando saltas al Logitech.

Respecto al ratón, ambos son muy cómodos, pero el Logitech se nota más ergonómico con diferencia. Además, no tiene un láser que haga daño a los ojos cuando le das la vuelta.

Ganador: **el teclado Microsoft gana por poco, y el ratón de Logitech gana con diferencia**.

## Veredicto final

Ganador: ambas opciones ganan y pierden en diferentes puntos. Poniendo todo en la balanza, para mí **el equipo de Logitech es el ganador**.
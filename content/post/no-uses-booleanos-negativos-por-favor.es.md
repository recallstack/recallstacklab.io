---
title: No uses booleanos negativos, por favor
date: 2020-09-01T15:32:30Z
tags:
  - humor
  - ideas
---

Odio los booleanos negativos.

Si alguna vez, por algún motivo cualquiera, necesitas usar una variable
[booleana](https://es.wikipedia.org/wiki/Tipo_de_dato_l%C3%B3gico) (algo habitual), por
favor **piensa siempre en positivo**.

<!-- more -->

No sabes la cantidad de neuronas que he malgastado interpretando código como este:

```python
# Declarar variable
no_actualizar = True

# Usar variable
if not no_actualizar:
  hacer_algo()
```

![¿Mandee?](https://media.giphy.com/media/JRJE0D11gFSJFRWhjz/giphy.gif)

O sea... ¿por qué no escribirlo así?

```python
# Declarar variable
actualizar = False

# Usar variable
if actualizar:
  hacer_algo()
```

Y vez tras vez sigo viendo ese patrón... En serio, siempre positivos por favor. Y si
tienes que negarlo, ¡pues pon un operador `not` delante, que para eso existen!

---
title: "Cómo hacer que Fish te avise al terminar un comando"
date: 2019-05-18T07:14:03+01:00
categories:
  - Programación
tags:
  - Fish
---

Quienes vivimos gran parte de nuestra jornada en la terminal es normal que tarde o
temprano acabemos usando una shell como [Fish](http://fishshell.com/), que nos hace la
vida más cómoda.

En mi caso muchas veces realizo tareas que toman cierto tiempo, y me viene bien que algo
me avise cuando terminan, para que mientras tanto pueda ir haciendo otras cosas.

Si usas GNOME Terminal y Bash o Zsh, esto viene activado por defecto en las últimas
versiones, pero si usas Fish, no. No sé si algún día corregirán
[ese problema](https://gitlab.gnome.org/GNOME/gnome-terminal/issues/54), pero mientras
tanto (y para quien no use esa terminal solamente), no estamos desamparados:

<!-- more -->

En todo terminal, el _prompt_ es el texto que te aparece para darte la bienvenida y que
escribas un comando. El mío, por ejemplo, tiene este aspecto:

<pre><font color="#8AE234">yajo</font>@yajolap:<font color="#4E9A06">~/D/d/p/recallstack.gitlab.io</font>|<font color="#CC0000">master⚡</font><font color="#C4A000">?</font>
➤
</pre>

Puedes personalizarlo cómodamente en Fish usando este comando y yendo a la pestaña
"prompt":

```fish
$ fish_config
```

Una vez lo tengas personalizado a tu gusto, se habrá creado el archivo
`~/.config/fish/functions/fish_prompt.fish`.

Edítalo, y añade
[este código](https://gitlab.com/yajoman/fish-config/blob/0f5482bcc177212827bfdab9b07b9aae3b51e0e7/functions/fish_prompt.fish#L2-24)
**al principio de la función `fish_prompt`**.

A partir de entonces, cuando un comando dure más de 10 segundos, recibirás una
notificación de escritorio con las siguientes características:

- Un sonido breve indicando si ha funcionado o no.
- El comando que se ha ejecutado.
- El tiempo que ha tomado.
- El código de salida (si ha fallado).
- Un icono fácilmente identificable para que sepas si ha fallado o no.
- La notificación desaparecerá tras unos segundos.

Funcionará en cualquier terminal que abras. Eso sí, lógicamente tienes que tener
instalados `notify-send` y `paplay`.

Observa:

{{% youtube M_-vNiJpPYQ %}}

---
title: Do not use negative booleans, please
date: 2020-09-01T15:32:30Z
tags:
  - humor
  - ideas
---

I hate negative booleans.

If any time, for any reason, you need to use a
[boolean](https://en.wikipedia.org/wiki/Boolean_data_type) variable (very common),
please **always think in positive**.

<!-- more -->

You don't know how many brain cells I have wasted interpreting code like this:

```python
# Declare variable
no_update = True

# Use variable
if not no_update:
  do_something()
```

![What The Fire?](https://media.giphy.com/media/JRJE0D11gFSJFRWhjz/giphy.gif)

I mean... why not just write it like this?

```python
# Declare variable
update = False

# Use variable
if update:
  do_something()
```

And time and again I keep on seeing that pattern... Seriously, always be positive
please. And if you need to negate it, then just put a `not` operator in front of it,
that's why they exist for!

---
title: Apps for video calls during your COVID-19 quarantine
slug: apps-video-calls-covid-19-quarantine
draft: false
date: 2020-03-19T16:21:31.000Z
tags:
  - google duo
  - jitsi meet
  - skype
  - facetime
  - zoom
  - whatsapp
nocomments: false
---

Half the planet is under quarantine, and the other half will be soon (no matter if you
still believe you won't)... but we want to keep in touch with our relatives! What
options do we have?

<!--more-->

## WhatsApp video calls

Facebook's option (remember it's WhatsApp's owner). Who doesn't have WhatsApp today?
(You lucky guy...)

{{< figure src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/WhatsApp_screenshot_rus.png/300px-WhatsApp_screenshot_rus.png" title="WhatsApp" link="https://www.whatsapp.com/" attr="Courtesy of Anslem Douglas and Wikimedia" attrlink="https://commons.wikimedia.org/wiki/File:WhatsApp_screenshot_rus.png" class="img-thumbnail" >}}

Advantages:

- High market share. Come on, almost 100%.
- Video and audio is OK.

Disadvantages:

- Propietary code.
- Group calls up to 4 people.
- You need to install the app in your phone.
- You need to have an account, giving out critical personal data such as your real
  mobile number, and allowing Facebook to read your whole address book.
- It's not multi-device. I mean that you have to call using the mini-screen of your
  mobile.
- It's not multi-platform. Just Android and iOS.
- You cannot see only who is talking. The screen is always divided in 4. And remember
  it's your mobile's min-screen.

Extra notes:

- [Instructions for video calls](https://faq.whatsapp.com/en/android/26000026/?category=5245237&lang=en).

## Skype

Microsoft's option. Very known, it's been among us for a long time.

{{< figure src="/assets/skype.png" title="Skype" link="https://www.skype.com/" class="img-thumbnail" >}}

Advantages:

- Good video quality.
- Good resilience in low bandwidth conditions.
- Up to 50 free participants.
- Multi-plataform.
- Allows to start calls.

Disadvantages:

- Propietary code.
- Hard to use.
- You need to install the app.
- You need to have a Microsoft account.
- You cannot enter a meeting from a link (AFAIK).
- Inefficient echo cancel. I mean: instead of filtering out input sounds that come from
  your speakers, it mutes all microphones except the one that sounds loudest. This makes
  that when someone whispers you, his/her mic could be closed, or when someone answers
  before you finished talking (bad habit, OTOH), both lose what the other one is saying.

Extra notes:

- [You can install Skype from FlatHub on Linux][skype-flathub]; it's better than from
  the official website.

## Zoom

Specifically designed for huuuge meetings, pro level.

{{< figure src="/assets/zoom.png" title="Zoom" link="https://zoom.us/" class="img-thumbnail" >}}

Advantages:

- Screen sharing from your desktop is an exquisite experience:
  - It puts all controls at the top of the screen comfortably hidden.
  - It allows you to see a freely-positionable meeting thumbnail.
  - In normal mode, video quality is prioritary, but in "video mode", the video quality
    will be adjusted to give priority to fluidity, automatically.
  - It allows to share just one window or desktop, even on Linux.
  - It allows to share desktop in Android or iOS.
  - It allows to define if you want to share your desktop sound or not.
- In its best version it reaches 1000 participants per meeting.
- Allows to start calls.
- Allows to join a meeting.
- Allows strict permissions in a meeting:
  - Who is host (there can be several).
  - Who can talk.
  - Kick out attendees.

Disadvantages:

- Propietary code.
- Free version only allows group meetings up to 40 minutes.
- You need to install the app.
- You need to have an account.

Extra notes:

- [You can install Zoom from FlatHub on Linux][zoom-flathub]; it's better than from the
  official website,
  [although screen sharing under Wayland is not working for now](https://github.com/flathub/us.zoom.Zoom/issues/22).

## FaceTime

For apple people.
{{< figure src="https://live.staticflickr.com/7248/7857087992_f48f2e3ba9.jpg" title="FaceTime" link="https://apps.apple.com/es/app/facetime/id1110145091" class="img-thumbnail" >}}

Advantages:

- Easy to use.
- You can put a giraffe on your head.
- You can call.

Disadvantages:

- Propietary code.
- Only one to one calls.
- Only on Apple devices.
- You cannot join a meeting.

## Google Duo

To never use the telephone again. One of my favourites. 😍

{{< figure src="https://duo.google.com/about/static/images/be-together.png" title="Google Duo" link="https://duo.google.com/" class="img-thumbnail" >}}

Advantages:

- Easy to use.
- You can call.
- It links with your address book to find Duo users, using your phone number. Just like
  WhatsApp.
- Apart from video calls, you can leave video messages when they cannot attend you.
- Multiplatform.
- You don't need to install it in your desktop.
- End-to-end encryption.
- Group video calls up to 12 people.
- Free.

Disadvantages:

- Propietary code.
- You need to have an account, giving out critical personal data such as your real
  mobile number, and allowing Google to read your whole address book.
- You cannot moderate group video calls.
- You cannot share your desktop.

## Jitsi Meet

My favorite. 😍

{{< figure src="https://jitsi.org/wp-content/uploads/2017/06/jitsi-front.png" title="Jitsi Meet" link="https://meet.jit.si/" class="img-thumbnail" >}}

Advantages:

- [Open source](https://github.com/jitsi/jitsi-meet).
- [No participants limit][jitsi-limit], as long as clients can support them.
- 100% free.
- Very easy.
- Allows to join a meeting.
- You don't need to have an account.
- You don't need to install an app (although for phones and tablets you better install
  it).
- It allows you to join a meeting by phone, with phone bridges in several countries,
  including a landline in Spain.

Disadvantages:

- Doesn't allow to start calls, instead you must share the meeting link with your peer
  through any other mean, and wait until (s)he connects.
- It works fine with any browser, but for now on ly Google Chrome/Chromium is officially
  supported.
- A slow computer could make your web experience not so performant as a native app
  would.

[skype-flathub]: https://www.flathub.org/apps/details/com.skype.Client
[zoom-flathub]: https://www.flathub.org/apps/details/us.zoom.Zoom
[jitsi-limit]: https://community.jitsi.org/t/participants-can-join-per-room/18983/4

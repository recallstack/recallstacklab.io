---
title: Cómo tomar notas
draft: false
nocomments: false
date: 2021-05-14T07:00:45.725Z
tags:
  - notas
  - zettelkasten
  - vscode
  - syncthing
  - gitjournal
  - git
  - foam
  - markdown
  - markor
  - mark-text
  - sleek
  - todo.txt
  - zettelkasten
  - joplin
---
Parece que tomar notas es una tontería, pero no es tan sencillo. Mis requisitos:

1. **Soberanía**. Mis notas deben ser mías. No quiero a ninguna corporación husmeándolas.
2. **Supervivencia**. Las aplicaciones de notas van cambiando... se venden y se compran, caducan, evolucionan... No me opongo a cambiar de aplicación en el futuro, pero no quiero tener que reescribir mis notas.
3. **Sincronización**. Quiero poder escribir en mi ordenador y leer en mi móvil, y viceversa. Utilizo varios sistemas operativos.
4. **Offline**. Si no tengo Internet, no quiero quedarme sin poder usarlas.

Puntos extra:

1. **Compartibles**. Si quiero anotar algo en la lista de la compra, quiero que el resto de mi familia también lo vea. Y quiero ver lo que ellos escriban.

   No es un requisito porque, para este uso concreto, puedo usar otra aplicación que sí permita la compartición, aunque no sea mi favorita. Al final, no tengo *tantas* notas compartidas.
2. **Fácil**. Quisiera poder convencer a otros para que usen mi sistema. Soy desarrollador, pero la mayoría de mis amigos son seres humanos.

   No lo considero un requisito porque puedo escribir código, ejecutar comandos, montar servidores... Pero me resulta una distracción innecesaria para el mero hecho de tomar notas.
3. **Alarmas**. Soy muy olvidadizo.

   No es requisito porque puedo usar otra aplicación para las alarmas.
4. **Formateables**. Puedo vivir con texto plano, pero... no es lo que más me gusta.
5. **Olvidables**. Si borro algo, quisiera que desaparezca para siempre.
6. **Mano alzada**. Si puedo dibujar con un bolígrafo en mi tablet, es un punto extra.
7. **[Zettelkasten](https://zettelkasten.de/)**. También conocido como segundo cerebro, o wiki local. Es un método de aprendizaje interesante, según el cual divides lo que aprendes en unidades pequeñas (ideas) que se entrelazan entre sí.

   No es un requisito porque estos sistemas te permiten aumentar tu conocimiento, pero no tu sabiduría, que es más importante.

<!-- more -->

Si destilamos los requisitos, vemos que se separan en sistemas de captura de notas y sistemas de sincronización, si bien es posible que ambos sistemas estén contemplados en la misma aplicación.

## Para los programadores 👨‍💻: GitJorunal + VSCode + Foam + Git

Salvando algunos bugs existentes, la experiencia ha sido bastante satisfactoria. Tiene algunos puntos buenos:

* Usamos [git](http://git-scm.com/) para sincronizar, con un potencial virtualmente infinito de organización y resolución de conflictos.
* Usamos [markdown](https://www.markdownguide.org/) para tomar las notas, completamente formateables.
* [GitJournal](https://gitjournal.io/) es bastante ergonómico en dispositivos móviles, y tiene un aspecto muy agradable:

  ![Diferentes editores en GitJournal](https://camo.githubusercontent.com/7aed535cef2c5eabdb4bab1663e0bfddb089aab2cfedd68256f39d02ef991799/68747470733a2f2f6769746a6f75726e616c2e696f2f73637265656e73686f74732f616e64726f69642f323032302d30362d30342f656e2d47422f696d616765732f70686f6e6553637265656e73686f74732f4e657875732036502d352e706e67)

  ![Listado de notas en GitJournal](https://camo.githubusercontent.com/f08d789b3573b5e185ed6ed8f0c83c9550639ca081c0dc4362e486392eb61f61/68747470733a2f2f6769746a6f75726e616c2e696f2f73637265656e73686f74732f616e64726f69642f323032302d30362d30342f656e2d47422f696d616765732f70686f6e6553637265656e73686f74732f4e657875732036502d31332e706e67)
* [VSCode](https://code.visualstudio.com/) es cómodo y rápido para escribir.
* [Foam](https://foambubble.github.io/foam/) permite mejorar bastante la experiencia de tomar notas y crear tu Zettelkasten en VSCode, y [recomiendan otras extensiones](https://foambubble.github.io/foam/recommended-extensions) que ayudan en ciertos aspectos en concreto:

  ![VSCode con Foam](https://raw.githubusercontent.com/foambubble/foam/deb77328c0526c1d3cdbfad9fc8c444d3a59df23/docs/assets/images/foam-features-dark-mode-demo.png)

No obstante, tras un tiempo usándolos, desistí. Motivos:

* El front matter YAML que usa GitJournal puede llegar a ser una distracción cuando estás en VSCode:

  ![Editando una nota en GitJournal, donde se ve el front matter YAML](https://camo.githubusercontent.com/3e9d1f44e8b134d617f46790a1dee24695fc25034ad524d7cb4f3adf959949a2/68747470733a2f2f6769746a6f75726e616c2e696f2f73637265656e73686f74732f616e64726f69642f323032302d30362d30342f656e2d47422f696d616765732f70686f6e6553637265656e73686f74732f4e657875732036502d362e706e67)
* VSCode tiene demasiadas opciones disponibles, que pueden llegar a distraer cuando estás tomando notas.
* Compatibilizar ambas aplicaciones manteniendo una experiencia de uso mínimamente homogénea toma su tiempo (especialmente a la hora de configurar VSCode). Particularmente, crear una nueva nota en VSCode de modo que sea usable directamente en GitJournal es complicado.
* Tener que resolver algún conflicto en git también distrae.
* Las notas no están encriptadas. Aunque siempre puedes crear un repositorio git en tu propio servidor, al cual accedes solo por SSH, y que esté en un disco encriptado. A efectos prácticos, es un problema menor.
* Git no permite olvidar. Si una vez escribiste algo, quedará en el histórico para siempre. Aunque borres notas, el espacio utilizado siempre aumentará. Este punto me parece un tanto crítico.

Probablemente, [una versión de escritorio de GitJournal](https://github.com/GitJournal/GitJournal/issues/137) simplificaría mucho este flujo de trabajo, pero no está disponible por ahora.

En definitiva, para reducir distracciones, espacio en disco, tiempo, y poder olvidar lo que ya no quiera, me tuve que decantar por otra opción.

## Para los semiprogramadores: Markor + Mark Text + Sleek + Syncthing

Tras desistir de la opción anterior, probé un sistema similar, pero que eliminara la necesidad de usar git y cualquier aplicación con demasiadas opciones que pudiera distraer, y llegué a esta combinación. Tiene ciertas ventajas sobre la anterior:

* [Syncthing](https://syncthing.net/) es una maravilla que está disponible en casi cualquier plataforma, y que nos permite sincronizar cualquier tipo de archivo sin demasiado esfuerzo, permitiendo guardar un histórico personalizable que permite también olvidar cosas, y con una resolución de conflictos menos exhaustiva que la de Git (más cómoda para este caso de uso). Cuando te habituas a él, empiezas a usarlo para todo:

  ![Syncthing, mostrando varios directorios sincronizándose con varios dispositivos](https://raw.githubusercontent.com/syncthing/docs/9c2e90cba6c58f9de3a6caa50faaa5a7042248e5/intro/gui1.png)
* [Markor](https://gsantner.net/project/markor.html?source=github) es bastante bueno para editar los formatos [markdown](https://www.markdownguide.org/) y [todo.txt](http://todotxt.org/) en Android:

  ![Editando markdown en Markor](https://raw.githubusercontent.com/gsantner/markor/b69da230c89ec1419c973ced23356ca5931f343e/metadata/en-US/phoneScreenshots/03.jpg)

  ![Editando todo.txt en Markor](https://raw.githubusercontent.com/gsantner/markor/b69da230c89ec1419c973ced23356ca5931f343e/metadata/en-US/phoneScreenshots/05.jpg)
* En el escritorio, [Mark Text](https://marktext.app/) y [Sleek](https://github.com/ransome1/sleek) son cómodos y bonitos a la hora de editar markdown y todo.txt respectivamente:

  ![Editando un archivo markdown en Mark Text](https://raw.githubusercontent.com/marktext/marktext/4dada5b84dfcf0546ed54f50b5cdb81fc9fc6a29/docs/themeImages/graphite-light.png)

  ![Editando un todo.txt en Sleek](https://raw.githubusercontent.com/ransome1/sleek/3e920fce6a789866f1d676dec2d01b807f689672/assets/screenshots/linux/todo_list.png)

Bueno, este sistema me dejó más satisfecho que el anterior. No obstante, aún tenía ciertas desventajas:

* Ese baile de cambiar entre 4 aplicaciones diferentes.
* Todo.txt me parece excelente, pero Markor no tiene alarmas al respecto, y Sleek no permite ordenar las tareas sin priorizarlas.
* Crear nuevas notas no es muy ergonómico en ninguno de los casos.
* Falta de encriptación. Como con el caso anterior, no es un gran problema.
* Todavía sentía que había demasiadas distracciones a mi alrededor.

Bueno, a seguir buscando...

## El ganador: Joplin

Ya había probado [Joplin](https://joplinapp.org/) anteriormente, pero lo había descartado porque no me terminó de gustar su aspecto:

![Joplin, mostrando sus aplicaciones para Android, terminal y escritorio](https://raw.githubusercontent.com/laurent22/joplin/ebf92605aeabbd686d57952ab6b946d2db8dcf27/docs/images/AllClients.jpg)

Craso error. Volví a darle una nueva oportunidad, investigando más a fondo las opciones que trae. Resultó cumplir con todas mis expectativas a un grado más que razonable:

* Utiliza markdown como formato interno, pero en el escritorio permite escribir usando un editor visual que te esconde por completo el código markdown. ¡Bien! Una distracción menos.
* Si no te gusta ninguno de sus editores, permite usar un editor externo.
* Código libre, con una comunidad muy activa.
* Organiza las notas por libretas.
* Una nota puede ser convertida en tarea.
* Sincronización integrada, compatible con diversos backends (sistema de archivos local, OneDrive, Dropbox, S3, WebDAV, o el propio Joplin Server (el más optimizado y que permite compartir notas)).
* Permite encriptar las notas al sincronizar, de modo que, incluso si sincronizas con un servicio que escapa a tu control, tus notas siguen siendo privadas.
* Permite configurar los días de historial a recordar.
* Permite exportar e importar múltiples formatos, incluido un directorio con archivos markdown.
* Permite adjuntar cualquier tipo de archivo a las notas. Soporta especialmente bien los vídeos, audios, imágenes y PDF.
* Permite usar un editor externo con los adjuntos, monitorizando sus cambios.
* Aplicaciones para casi cualquier sistema operativo.
* Muchos atajos de teclado y buscador de notas y de comandos parecido al de VSCode.
* Permite configurar una alarma por nota.
* Incluso su uso más avanzado resulta sencillo.
* Realmente ha aumentado muchísimo mi productividad.

¡Bien por Joplin! La experiencia me está gustando muchísimo. Se ha convertido en uno de esos proyectos que me entusiasman. De hecho, ¡ya alguna familiar (humano 😯) me ha pedido que le enseñe a usarlo!

No obstante, tiene sus desventajas:

* No soporta todo.txt. Con algo de creatividad, puedes organizar tus quehaceres con criterios similares en markdown.
* El editor en Android tiene ciertos errores y es solo de texto plano. No es muy ergonómico. Por ahora me las apaño tomando solo notas básicas, y ya al sincronizar en el PC les doy formato si es necesario.
* [No soporta escritura a mano alzada](https://github.com/laurent22/joplin/issues/582), al menos por ahora.
* Para compartir notas entre varios usuarios, necesitas usar el backend Joplin Server, que requiere instalarlo y alojarlo en algún servidor.

## Otras opciones disponibles

Existen docenas de otros sistemas que he probado y descartado. Obviamente, mis pruebas no han podido ser muy exhaustivas dada la inmensa cantidad de alternativas. Algunos los he descartado por diferencias filosóficas con el proyecto, otras por no estar suficientemente maduros o no cumplir mis criterios.

Bueno, los coloco por aquí como futura referencia, o por si a alguien no le convence ninguno de los anteriores, ordenados alfabéticamente:

* [Boost Note](https://boostnote.io/)
* [Carnet](https://getcarnet.app/)
* [Cherrytree](https://www.giuspen.com/cherrytree/)
* [Crossnote](https://crossnote.app/)
* [Evernote](https://evernote.com/)
* [GNOME Notes](https://wiki.gnome.org/Apps/Notes)
* [Google Keep](http://keep.google.com/)
* [Laverna](https://laverna.cc/)
* [Notable](https://notable.app/)
* [Notion](https://www.notion.so/)
* [QOwnNotes](https://www.qownnotes.org/)
* [RedNotebook](https://rednotebook.sourceforge.io/)
* [Samsung Notes](https://www.samsung.com/global/galaxy/apps/samsung-notes/)
* [Simplenote](https://simplenote.com/)
* [Standard Notes](https://standardnotes.org/)
* [TagSpaces](https://www.tagspaces.org/)
* [TiddlyRoam](https://tiddlyroam.org/)
* [Tomboy](https://github.com/tomboy-notes/tomboy-ng)
* [Trilium Notes](https://github.com/zadam/trilium)
* [Turtl](https://turtlapp.com/)
* [Unotes](https://github.com/ryanmcalister/unotes)
* [WizNote](https://www.wiz.cn/)
* [Zettlr](https://www.zettlr.com/)
* [Zim Wiki](https://zim-wiki.org/index.html)

Por supuesto, si ninguna te convence, siempre puedes usar un cuaderno y un bolígrafo. 😃🗒️🖊️
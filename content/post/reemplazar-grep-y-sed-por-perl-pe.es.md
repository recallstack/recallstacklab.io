---
title: Reemplazar grep y sed por perl -pe
draft: false
nocomments: false
date: Su/03/yyyyT09:24
tags:
  - linux
  - regex
---
¿Sabías que puedes reemplazar `grep` y `sed` por `perl -pe` y tener todo el poder de los regex de Perl?

Ejemplos:

<!-- more -->

Supongamos que tienes un archivo con nombres de módulos de Odoo separados por coma, del que quieres extraer todos aquellos que:

1. No tengan relación con `payment`.
2. Sean de una localización que no sea la de España.

Usando solo `grep` o `sed` es muy complicado porque no tienen *[negative lookaheads](https://regextutorial.org/positive-and-negative-lookahead-assertions.php)* (búsqueda anticipada negativa). En cambio, con Perl es "muy" fácil:

```
$ cat example.txt
account,account_payment,l10n_es,l10n_es_reports,l10n_mx,l10n_mx_reports,l10n_se,l10n_se_reports,payment

$ perl -pe "s/,?\w*(l10n_(?!es_?)|payment)\w*//g" example.txt
account,l10n_es,l10n_es_reports
```

---
title: Cómo evitar el parpadeo en tu webcam
draft: false
nocomments: false
date: 2021-12-21T18:52:57.222Z
tags:
  - webcam
  - windows
  - linux
  - udev
  - toolbx
  - logitech
---
A veces puedes tener una webcam en la que se observa un parpadeo ("flickering" en inglés).

¿Cómo resolverlo?

<!-- more -->

En mi caso, tengo una *Logitech C920 HD Pro* y me producía ese problema.

### Por qué ocurre

Normalmente es porque la cámara está trabajando a 60 Hz y el monitor a 50, o viceversa.

## Cómo resolverlo en Windows

Para mi cámara en concreto, fue sencillo:

1. Descargar e instalar [la aplicación *Camera Settings*](https://support.logi.com/hc/es/articles/360024849133--Descargas-HD-Pro-Webcam-C920).
2. Abrirla.
3. Configurar la tasa de refresco de la cámara para coincidir con la del monitor.

## Cómo resolverlo en Linux

En Linux también es sencillo de hacer si instalas [el paquete `gtk-v4l`](https://pkgs.org/download/gtk-v4l). Si estás en Fedora 35:

```shell
# Instalar el paquete
sudo dnf -y install gtk-v4l

# Ejecutar la aplicación (también la encontrarás en el menú)
gtk-v4l
```

Te abrirá una interfaz gráfica desde la cual alterar ese parámetro ("Power Line Frequency" en la foto) y muchos otros:

![gtk-v4l UI](/assets/captura-de-pantalla-de-2021-12-21-18-46-18.png)

**La dificultad estriba en hacer ese cambio permanente**, ya que lo que cambies aquí se deshará tras tu próximo reinicio.

Para ello, es necesario crear una regla `udev`. En [este vídeo](https://www.youtube.com/watch?v=DaZ9zU3tdFY) y [este blog](https://blog.christophersmart.com/2017/02/07/fixing-webcam-flicker-in-linux-with-udev/) (ambos en inglés) explican muy bien cómo "cocinar" tu regla, así que paso a poner aquí la mía:

```
SUBSYSTEM=="video4linux", \
SUBSYSTEMS=="usb", \
ATTRS{product}=="HD Pro Webcam C920", \
ATTRS{idProduct}=="082d", \
ATTRS{idVendor}=="046d", \
PROGRAM="/usr/bin/v4l2-ctl --device $devnode --set-ctrl power_line_frequency=1"
```

La guardo en `/etc/udev/rules.d/100-yajo-logitech-webcam.rules`. Obviamente, para esto necesitamos tener instalado `v4l2-ctl`, así que para ello:

```sh
sudo dnf install -y v4l-utils
systemctl reboot
```

Tras el reinicio, tu webcam estará funcionando como debe.
---
title: "Cómo migrar de wordpress.com a Hugo"
slug: como-migrar-de-wordpress-com-a-hugo
date: 2018-10-16T15:13:48+01:00
categories:
  - Opinión
  - Tutoriales
tags:
  - Blog
  - Gitlab
  - Hugo
  - Jekyll
  - Wordpress
---

Si sigues mi blog habrás notado que desde hoy tiene un aspecto bastante diferente desde
hace poco, por no decir que ahora... ¡Tengo dominio propio!:
[RecallStack.icu](https://www.recallstack.icu)

Bueno, pues lo que hay por detrás de esto ya lo explicaré en otra entrada, pero la
cuestión es que he migrado de [Wordpress](https://wordpress.org/) a
[Hugo](https://gohugo.io/).

Más concretamente, he migrado de [wordpress.com](https://wordpress.com) a Hugo, lo cual
lo hace un poco más complicado (y caro), pero no mucho.

Quería **conseguir el mayor control posible con el mínimo coste posible**. En total me
he gastado 14,02 € el primer año, y en años sucesivos será alrededor de 6-8€ por año.

Te voy a contar cómo hacerlo, por si te interesa:

1. Escoger un alojamiento donde publicar el blog. Uno de los más usados es
   [GitHub pages](https://pages.github.com/), aunque me decanté por
   [Gitlab pages](https://about.gitlab.com/features/pages/), por unos motivos muy
   sencillos:

   - [Gitlab es de código abierto](https://gitlab.com/gitlab-org/gitlab-ce/).
   - Gitlab CI es una maravilla.
   - El [Web IDE de Gitlab](https://docs.gitlab.com/ce/user/project/web_ide/) me permite
     escribir borradores de entradas del blog desde cualquier navegador.
   - Gitlab me encanta.

   Por tanto, el resto del tutorial hablará de cómo desplegar en Gitlab Pages.

1. Como las páginas de Gitlab van con la estructura `nombre-de-usuario.gitlab.io`, mejor
   crea un equipo que te dé un nombre interesante. Por ejemplo, yo creé el equipo
   `recallstack`.

1. Clonar [el repositorio de ejemplo para Hugo](https://gitlab.com/pages/hugo) dentro de
   ese equipo. Repasa el README del proyecto, que te explica unas cuantas cosas que
   deberías hacer después.

1. Necesitas exportar la información de Wordpress, como ves aquí:

   ![Exportar datos de wordpress](/assets/wordpress-exportar.gif)

   Esto te generará un fichero XML con todo el contenido que tenías en tu blog en
   Wordpress.

1. Ahora tienes que procesar ese XML para importarlo a [Jekyll](https://jekyllrb.com/),
   siguiendo
   [las instrucciones que hay en su página web](https://import.jekyllrb.com/docs/wordpressdotcom/).

   Este proceso puede parecer un tanto extraño, pero
   [está entre los recomendado por la web de Hugo](https://gohugo.io/tools/migrations/#wordpress),
   aunque si prefieres probar otro sistema, hay otros.

   Por cierto, Jekyll es otro SSG (_Generador de Sitios Estáticos_, por sus siglas en
   inglés) parecido a Hugo. Me gustó más Hugo.

1. Importa ahora el sitio que se te ha generado para Jekyll en Hugo:

   ```bash
   hugo import jekyll carpeta/jekyll carpeta/hugo
   ```

   Llegados a este punto, ya tienes la mayoría del trabajo hecho.

1. [Escoger un tema](https://themes.gohugo.io/) e
   [instalarlo](https://gohugo.io/themes/installing-and-using-themes/).

1. Cambiar cualquier otra cosa que no te interese de ese repositorio de ejemplo.

1. Habrá ciertos detalles que tengas que cambiar, y lo puedes hacer masivamente mediante
   el uso de expresiones regulares y tal. Te comento algunos que me encontré:

   - En Hugo, el autor es un string, mientras que en Jekyll se importaba como un array.
     Afortunadamente, casi todas las entradas de mi blog eran del mismo autor, así que
     lo puse en mi `config.yaml` y lo eliminé de esos ficheros usando algunos comandos
     de terminal y la imaginación. En las pocas entradas que no eran de mi autoría, lo
     puse a mano.

   - Hugo autogeneraba los slugs de las entradas. Para hacer redirecciones funcionales,
     necesito que las URL sean idénticas a las que había en Wordpress (al menos la
     mayoría). Añadí esto a mi `config.yaml`:

     ```yaml
     permalinks:
       post: /:year/:month/:day/:slug
     ```

     También tuve que añadir el slug manualmente, obteniéndolo de los nombres de
     archivo. Nuevamente la terminal y algo de imaginación resolvieron el problema en
     poco tiempo.

1. Subir tus cambios. Automáticamente, Gitlab CI te creará un sitio web en
   `tu-grupo.gitlab.io`.

1. Claro, lo que yo quería era tener mi propio dominio, así que vamos a
   [Namecheap](https://namecheap.com) y compramos un dominio cualquiera. De ahí esa
   extensión `.icu` tan rara: era la más barata para comprar y renovar (ojo, porque
   siempre las renovaciones serán más caras, para "engancharte"; en Namecheap aparece
   como "Retail").

   No te calientes mucho la cabeza configurando los DNS porque en breve los cambiaremos.

1. También quiero tener un certificado para poder usar `https://` en mi blog, y
   [mientras que Gitlab no soporte automáticamente la descarga de certificados por Let's Encrypt](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996),
   parece que la vía más sostenible es usar [CloudFlare](https://www.cloudflare.com/),
   así que vamos a su web y abrimos una cuenta.

1. Verás unas instrucciones sobre cómo cambiar los nameservers de tu dominio, así que
   vuelve a la Namecheap y cámbialos. Este cambio tardará unos minutos, o puede que
   hasta un día, en hacerse efectivo.

1. También en CloudFlare puedes usar las _Page Rules_ para configurar una redirección
   del dominio raíz (`recallstack.icu` en mi caso) al subdominio principal
   (`www.recallstack.icu` en mi caso):

   - Patrón: `https://recallstack.icu/*` (en mi caso)
   - Acción: Forwarding URL - 301 - Permanent redirect -
     `https://www.recallstack.icu/$1`

   Nota: utilizar un subdominio tiene la ventaja de poder poner un CNAME, cosa imposible
   en un dominio raíz. Por eso recomiendo usarlo por defecto, salvo que tengas otro
   motivo importante para no hacerlo.

1. Cuando el cambio de nameserver sea efectivo, vuelve a CloudFlare y
   [sigue estas instrucciones para configurarlo, obtener el certificado y clave privada, y configurarlos en Gitlab](https://about.gitlab.com/2017/02/07/setting-up-gitlab-pages-with-cloudflare-certificates/).

   Desde Gitlab también puedes forzar el uso de HTTPS.

1. No es que el posicionamiento me quite el sueño, pero ver mi blog entre los primeros
   al hacer ciertas búsquedas siempre da gustito, así que ahora toca avisar a mis amigos
   de Wordpress que ya no van a recibir actualizaciones por ahí. Simplemente publiqué
   una entrada breve para que quienes me siguen por RSS sepan que a partir de ahora me
   he mudado de dominio.

1. También para poder preservar el posicionamiento, una de las herramientas básicas es
   las redirecciones 301. Wordpress.com no es tonto, y si quieres irte te cobra por
   colocar esas redirecciones. La parte buena es que no he tenido que contratar un
   paquete especial, sino que tienen un
   [pack de solo redirección que cuesta apenas 13€/año](https://en.support.wordpress.com/site-redirect/).
   Contraté ese pack y es bastante sencillo de configurar. Como ya tenía Hugo
   configurado para tener slugs iguales a los de Wordpress.

1. Añadirme al calendario un recordatorio para eliminar el blog de Wordpress dentro de
   unos 10 meses, antes de que se renueve ese contrato.

1. Antes tenía vinculada mi cuenta de Twitter a la de Wordpress. Ahora lo hago con
   [IFTTT](https://ifttt.com/), usando el RSS del nuevo blog.

He perdido algunas cosas:

- Los comentarios ahora están en [Disquss](https://disqus.com/), así que los antiguos
  comentarios se han perdido. Pido disculpas a mis anteriores comentaristas, pero no vi
  forma de importarlos. 😅
- Ciertas URL son inmapeables a la nueva dirección, así que ahora darán error 404. Como
  decía, tampoco es que me obsesione el posicionamiento.
- No tengo estadísticas. Ya las pondré.
- No tengo anuncios. Pensaré si ponerlos, pero si pongo, esta vez **seré yo quien los
  cobre** 😎. Lástima que la mayoría de mis lectores seguramente tengan un bloqueador de
  anuncios ya puesto. 🤷‍

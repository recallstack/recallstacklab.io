---
title: How to create a Windows virtual machine inside Linux
date: 2020-10-02T20:24:20Z
tags:
  - GNOME Boxes
  - Linux
  - SPICE
  - Virtualización
  - Windows
  - Wine
---

Having a Windows virtual machine in Linux has a lot of utilities:

1. If you're testing Linux for the first time, it can help you "unhook yourself" from
   Windows little by little. You know you have Windows there, and that placebo gives you
   the necessary courage to test this little weird new world that is Linux.
1. You're a developer and you want to see how your apps or services work on Windows.
1. You need to use one app that has no native Linux alternative and that you cannot run
   with [Wine](https://www.winehq.org/).
1. You need to use one app that is only available through
   [Windows app store](https://www.microsoft.com/store/apps/windows).

It turns out getting it is way easier than what you imagine:

<!-- more -->

1.  **Install a virtual machine manager.**

    I will use "Boxes". You'll have to install it using your distribution's package
    manager. The package is most likely named `gnome-boxes`.

    Beware: do not install it from [Flathub](https://flathub.org/) because it still has
    [a problem with network management](https://gitlab.gnome.org/GNOME/gnome-boxes/-/issues/232)).

    For myself, in Fedora Silverblue, the command to run is this, which besides will
    reboot the system after installing:

    ```bash
    rpm-ostree install --reboot gnome-boxes
    ```

    If you use another distro that isn't based on
    [OSTree](https://ostree.readthedocs.io/en/latest/), probably you will not need to
    reboot. Actually, it will be as easy as searching for "Boxes" in the app store and
    install it:

    {{< figure src="/assets/gnome-boxes-install-in-ubuntu-20-04.gif" caption="Installing Boxes in Ubuntu 20.04" >}}

    In [pkgs.org](https://pkgs.org/search/) you can obtain more details about
    [where to find that package for each distro](https://pkgs.org/search/?q=gnome-boxes).

1.  **Download the Windows virtual machine.**

    For that, visit one of these, depending on if you want a full development
    environment, or if you are just going to use it to install a few programs:

    - [Full Windows 10 development environment](https://developer.microsoft.com/windows/downloads/virtual-machines/)
      (about 20 GiB).
    - [Windows 10 with MSEdge](https://developer.microsoft.com/microsoft-edge/tools/vms/)
      (about 8 GiB).

    Download the most recent version possible for VirtualBox.

    They include a valid 90 days Windows license, so you'll have to repeat this process
    every now and then.

1.  **Decompress it:**

    {{< figure src="/assets/windows-vm-unzip.gif" caption="Decompressing the Windows virtual machine" >}}

1.  **Open "Boxes" and create a new box using that file:**

    {{< figure src="/assets/windows-vm-create.gif" caption="Creating the Windows virtual machine" >}}

    Note: Windows alone will already consume about 3 GiB of memory, so better assign to
    it at least 6, so the apps you open can breath a little bit.

1.  **Configure language and keyboard layout inside the box**, for convenience:

    {{< figure src="/assets/windows-vm-spanish.gif" caption="Configuring keyboard layout and regional settings in the Windows virtual machine" >}}

1.  **Install [the SPICE extensions](https://www.spice-space.org/download.html) in
    Windows** to let it integrate better with your host machine. It allows stuff such as
    detecting screen resolution, sharing the clipboard or avoiding to have to press
    <kbd>Ctrl</kbd>+<kbd>Alt</kbd> to release the mouse:

    {{< figure src="/assets/windows-vm-spice-guest-tools-installation.gif" caption="Installing SPICE extensions in the Windows virtual machine" >}}

Ready! You already have a Windows there to do whatever you want.

---
title: How to delete Ubuntu old kernels when apt-get autoremove gets broken
date: 2020-08-31T21:06:57Z
tags:
  - apt-get
  - Linux
  - Ubuntu
---

What to do when you find this 💩 in the terminal?

```bash
➤ sudo apt-get remove linux-image-generic
Reading package lists... Done
Building dependency tree
Reading state information... Done
You might want to run 'apt-get -f install' to correct these:
The following packages have unmet dependencies:
 linux-generic : Depends: linux-image-generic (= 4.4.0.143.151) but it is not going to be installed
 linux-modules-extra-4.4.0-143-generic : Depends: linux-image-4.4.0-143-generic but it is not going to be installed o
                                                  linux-image-unsigned-4.4.0-143-generic but it is not going to be installed
 linux-signed-image-generic : Depends: linux-image-generic but it is not going to be installed
E: Unmet dependencies. Try 'apt-get -f install' with no packages (or specify a solution).
```

It's a little hard, but let's go...

<!-- more -->

## What's happening?

You'll see it quickly if you run this:

<pre>
➤ sudo df -h | grep /boot
/dev/md126p2                              473M   473M     0 <b>100% /boot</b>
/dev/md126p1                              513M   3,4M  509M   1% /boot/efi
</pre>

Can you see that nice `100% /boot`? It's telling you that there's no more room for
kernels in the boot partition.

Ubuntu is so `<sarcasm>`wonderful`</sarcasm>` that it lets you keep accumulating kernels
infinetly. Besides, `/boot` partition recommended size is quite small by default. And
also, when it gets full, you cannot run `sudo apt-get autoremove` to delete old kernels
because, since it cannot build a correct dependency tree, it fails before being able to
delete anything.

## The solution

Let's go step by step:

### 1. Free space in `/boot`

To that, first of all, you need to know which kernel are you using:

```bash
➤ uname -r
4.4.0-142-generic
```

Now that this is clear, remember: **never delete that kernel or any other with a higher
version**.

Now let's see what's filling up `/boot`:

```bash
➤ ls -lh /boot/initrd.img-*
-rw-r--r-- 1 root root 9,3M dic 15  2018 /boot/initrd.img-4.4.0-109-generic
-rw-r--r-- 1 root root  39M dic 15  2018 /boot/initrd.img-4.4.0-130-generic
-rw-r--r-- 1 root root  39M dic 15  2018 /boot/initrd.img-4.4.0-133-generic
-rw-r--r-- 1 root root  39M dic 15  2018 /boot/initrd.img-4.4.0-134-generic
-rw-r--r-- 1 root root  39M dic 15  2018 /boot/initrd.img-4.4.0-137-generic
-rw-r--r-- 1 root root  39M dic 15  2018 /boot/initrd.img-4.4.0-138-generic
-rw-r--r-- 1 root root  39M dic 15  2018 /boot/initrd.img-4.4.0-139-generic
-rw-r--r-- 1 root root  39M ene 12  2019 /boot/initrd.img-4.4.0-141-generic
-rw-r--r-- 1 root root  39M feb  7  2019 /boot/initrd.img-4.4.0-142-generic
```

In my case, I can delete all of these except the last one (which I'm using), but really
just by deleting 3 or 4 it's gonna leave probably enough free room. Let's go. ⚠ Be
careful, this is a destructive operation:

```bash
➤ rm /boot/initrd.img-4.4.0-109-generic /boot/initrd.img-4.4.0-130-generic /boot/initrd.img-4.4.0-133-generic /boot/initrd.img-4.4.0-134-generic
```

We see there's now free space:

<pre>
➤ sudo df -h | grep /boot
/dev/md126p2                              473M   349M  100M  <b>78% /boot</b>
/dev/md126p1                              513M   3,4M  509M   1% /boot/efi
</pre>

### 2. Rebuild the apt index

Now, for [apt](https://en.wikipedia.org/wiki/Advanced_Packaging_Tool) to stop bothering
us, we have to follow apt's sugestion from the 1st message above:

```bash
➤ sudo apt-get -f installe
```

This will install the new dependencies and rebuild the boot menu. If it failed, the
solution is to delete some other `/boot/initrd.img-*`, to free more space.

If it worked, let's check apt is happy again:

```bash
➤ sudo apt-get check
Leyendo lista de paquetes... Hecho
Creando árbol de dependencias
Leyendo la información de estado... Hecho
```

### 3. Delete old kernels, this time using apt

Effectively, now apt thinks there exist some files that don't really exist because we
deleted them, cowboy style 🤠, so let's ask apt to delete old kernels, official style
👮:

```bash
➤ sudo apt-get autoremove --purge
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages will be REMOVED:
  linux-headers-4.4.0-130* linux-headers-4.4.0-130-generic* linux-headers-4.4.0-133* linux-headers-4.4.0-133-generic* linux-headers-4.4.0-134* linux-headers-4.4.0-134-generic* linux-headers-4.4.0-137* linux-headers-4.4.0-137-generic*
  linux-headers-4.4.0-138* linux-headers-4.4.0-138-generic* linux-headers-4.4.0-139* linux-headers-4.4.0-139-generic* linux-headers-4.4.0-141* linux-headers-4.4.0-141-generic* linux-image-4.4.0-130-generic* linux-image-4.4.0-133-generic*
  linux-image-4.4.0-134-generic* linux-image-4.4.0-137-generic* linux-image-4.4.0-138-generic* linux-image-4.4.0-139-generic* linux-image-4.4.0-141-generic* linux-image-extra-4.4.0-130-generic* linux-image-extra-4.4.0-133-generic*
  linux-image-extra-4.4.0-134-generic* linux-image-extra-4.4.0-137-generic* linux-image-extra-4.4.0-138-generic* linux-image-extra-4.4.0-139-generic* linux-image-extra-4.4.0-141-generic* linux-signed-image-4.4.0-130-generic*
  linux-signed-image-4.4.0-133-generic* linux-signed-image-4.4.0-134-generic* linux-signed-image-4.4.0-137-generic* linux-signed-image-4.4.0-138-generic* linux-signed-image-4.4.0-139-generic* linux-signed-image-4.4.0-141-generic*
0 upgraded, 0 newly installed, 35 to remove and 73 not upgraded.
After this operation, 2119 MB disk space will be freed.
Do you want to continue? [Y/n]
```

If we agree with that list (which seem to be all old kernels), we press <kbd>Enter</kbd>
and apt will do the rest.

Reboot if you want, to use some new kernel you might have installed.

Ready!

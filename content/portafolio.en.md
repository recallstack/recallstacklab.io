---
title: Portfolio
slug: portfolio
nocomments: true
date: Th/05/yyyyT15:22
---
Here you have some of the projects where I have collaborated most:

## Odoo

Open source integral company management software.

* [My contributions](https://github.com/odoo/odoo/commits?author=Yajo)
* [I was part of the team that won the 2017 best contributor award](https://twitter.com/Odoo/status/915979619697819648).
* [Nominated as best contributor in 2018](https://www.odoo.com/blog/odoo-news-5/post/introducing-this-year-s-awards-nominees-for-odoo-experience-2018-505).

## Doodba

A family of projects destined to turn deploying [Odoo](https://www.odoo.com/) a piece of
cake. One of the pioneer projects to use containers for that. I'm the main designer and
maintainer of this project.

* https://github.com/Tecnativa/doodba
* https://github.com/Tecnativa/doodba-copier-template
* https://github.com/Tecnativa/doodba-qa

## Copier

Advanced templates generator with good updates and multiple templates support. I'm a
co-maintainer. \[Read why]({{< relref "/post/copier-v3-introduccion.md" >}}).

* https://github.com/copier-org/copier
* https://github.com/Tecnativa/doodba-copier-template
* https://github.com/Tecnativa/image-template
* https://gitlab.com/moduon/odoo-module-template

## Docker images

* https://github.com/Tecnativa/docker-socket-proxy - Unleash the power of exposing your Docker socket without unleashing its insecurity.
* https://github.com/Tecnativa/docker‐whitelist - A netcat service to whitelist network connections.

## Flathub

I maintain some projects:

* [KmCaster](https://github.com/flathub/com.whitemagicsoftware.kmcaster)
* [Not Tetris 2](https://github.com/flathub/net.stabyourself.nottetris2)

## This blog

This blog you're reading. You can send me patches if you want.

* https://gitlab.com/recallstack/recallstack.gitlab.io

## Various achievements

Here are some of the achievements I have made. I think it is interesting to share them, as a resume.

* December 2020. [I am interviewed on Python Podcast to talk about Copier](https://www.pythonpodcast.com/copier-project-scaffolding-episode-297/).
* February 2020. [The community likes my Doodba project](https://twitter.com/PCatinean/status/1225795167103455234).
* January 2020. [I am voluntarily appointed Copier maintainer](https://github.com/copier-org/copier/pull/109#issuecomment-576425526).
* June 2019. [I program a complex Bootstrap v3 to v4 converter for Odoo migrations from v11 to v12](https://github.com/OCA/openupgradelib/pull/148).
* January 2019. [80% performance improvement in Odoo `contract` module](https://github.com/OCA/contract/pull/266).
* October 2019. [Program a way to upgrade Odoo without downtime](https://github.com/acsone/click-odoo-contrib/pull/38).
* October 2018. [Migration with epic refactoring of the `web_responsive` module](https://github.com/OCA/web/pull/1066).
* August 2018. [Nominated as best contributor in 2018](https://www.odoo.com/es_ES/blog/nuestro-blog-5/post/presentando-a-los-nominados-de-este-ano-para-los-premios-de-odoo-experience-2018-505) (repeated above, I know).
* May 2018. [I help diagnose a serious 3-year-old bug](https://github.com/wkhtmltopdf/wkhtmltopdf/issues/2711#issuecomment-389155264).
* December 2017. [I develop the `website_form_builder` module](https://github.com/OCA/website/pull/402).
* October 2017. [I was part of the team that won the best contributor award 2017](https://twitter.com/Odoo/status/915979619697819648) (repeated also).
* June 2017. [Presentation on how to use Doodba to launch a custom Odoo project in 10 minutes](https://www.youtube.com/watch?v=d8wIygoGuuM).
* August 2016. [I demonstrate and fix a serious bug in Odoo ORM](https://github.com/odoo/odoo/pull/13082).
* December 2011. [My brief collaboration with the "Usemos Linux" blog, explaining how to make an RPM package](http://usemoslinux.blogspot.com/2011/12/empaquetado-rpm-parte-4-empaquetando.html).

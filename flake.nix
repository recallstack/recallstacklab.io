{
  description = "ReCallStack, my personal blog";

  nixConfig = {
    # HACK https://github.com/NixOS/nix/issues/6771
    # TODO Leave only own cache settings when fixed
    extra-trusted-public-keys = [
      "copier.cachix.org-1:sVkdQyyNXrgc53qXPCH9zuS91zpt5eBYcg7JQSmTBG4="
      "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw="
      "moduon.cachix.org-1:sXMrTN5LuhZyh6CnzYXM4pZkah/Yy//eGKt1oOZc0zw="
      "numtide.cachix.org-1:2ps1kLBUWjxIneOy1Ik6cQjb41X0iXVXeHigGmycPPE="
    ];
    extra-substituters = [
      "https://copier.cachix.org"
      "https://devenv.cachix.org"
      "https://moduon.cachix.org"
      "https://numtide.cachix.org"
    ];
  };

  inputs = {
    devshell.url = "github:numtide/devshell";
    flake-compat.url = "github:edolstra/flake-compat/refs/pull/63/head"; # HACK
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "nixpkgs";
  };

  outputs = inputs:
    with inputs;
      {
        devshellModules.hugo = {pkgs, ...}: {
          commands = [
            {package = pkgs.hugo;}
          ];
        };
      }
      // flake-utils.lib.eachDefaultSystem (system: let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            precommix.overlays.default
            devshell.overlays.default
          ];
        };
        precommix = import ./precommix.nix;
      in {
        devShells.default = pkgs.devshell.mkShell {
          imports = [
            precommix.devshellModules.default
            self.devshellModules.hugo
          ];
          env = [
            {
              name = "HUGO_BUILDDRAFTS";
              value = "true";
            }
            {
              name = "HUGO_BUILDFUTURE";
              value = "true";
            }
          ];
        };
        devShells.ci = pkgs.devshell.mkShell {
          imports = [
            precommix.devshellModules.default
            self.devshellModules.hugo
          ];
        };
      });
}

# ReCallStack

![Estado de construcción](https://gitlab.com/recallstack/recallstack.gitlab.io/badges/master/build.svg)

Este proyecto será mi futuro blog personal.

Está construido con [Harp](http://harpjs.com/) usando GitLab Pages.

Para levantar el proyecto localmente:

    docker-compose up -d

Una vez hecho eso, navega hacia http://localhost:9000 para ver la web.

## Licencia

Esta obra está sujeta a la licencia
[Reconocimiento-CompartirIgual 4.0 Internacional](http://creativecommons.org/licenses/by-sa/4.0/)
de Creative Commons.

# Get precommix from separate location, synced with last template update
import (builtins.fetchGit {
  url = "https://gitlab.com/moduon/precommix.git";
  ref = "main";
  rev = "1f7045c3c22c8b0a79baf8e7e48574d11f40eb13";
})

---
title: {{replace .Name "-" " " | humanize}}
date: {{.Date}}
draft: true
tags:
  {{- range .Site.Taxonomies.tags }}
  - {{ .Page.Title }}
  {{- end }}
---

Una introducción con gancho.

<!-- more -->

## No poner títulos mayores que H2
